<html>
    <head>
        <title>Painel - Rastreio</title>
        <script src="../assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="../assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="../assets/css/painel.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script>

            var marker;
            var infos;

            function initMap() {

                iconBase = "http://rastreio.injetec.com.br/assets/img/";


                var myLatLng = {lat: <?php echo $ultima_transmissao['latitude']; ?>, lng: <?php echo $ultima_transmissao['longitude']; ?>};

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 16,
                    center: myLatLng
                });

                infos = "<h5>Máquina: <?php echo $nome_maquina_sis_antigo; ?></h5>\n\
                        <ul>\n\
                            <li>Horímetro : <?php echo $ultima_transmissao['horimetro_horas']; ?> Hrs</li>\n\
                            <li>Operador : <?php echo $ultima_transmissao['driver_id']; ?></li>\n\
                            <li>Última Comunicação : <?php echo $ultima_transmissao['data_ultima_atualizacao']; ?></li>\n\
                            <li>Velocidade Atual : <?php echo $ultima_transmissao['velocidade_arredondada']; ?> Km/h</li>\n\
                            <li>Hodômetro : <?php echo $ultima_transmissao['hodometro_km']; ?> Km</li>\n\
                            <li>Tensão de bateria : <?php echo $ultima_transmissao['tensao_main_bat']; ?> V</li>\n\
                        </ul>";

                var infowindow = new google.maps.InfoWindow({
                    content: infos
                });

                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: 'Empilhadeiras',
                    icon: iconBase + 'weightlifting7.png',
                    animation: google.maps.Animation.DROP,
                });

                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });

            }

        </script>

    </head>
    <body>
        <?php $this->load->view("tool_bar.php"); ?>
        <div class="content-fluid" id="wrapper" >
            <div id="map"></div>
            <div>Icons made by <a href="http://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a>             is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></div>
        </div>
        <?php $this->load->view("modal/cadastro_condutor"); ?>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?signed_in=true&callback=initMap">
        </script>
    </body>



</html>
