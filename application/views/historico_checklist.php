<html>
    <head>
        <title>Checklist - Monitor</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/historico_checklist.css" />
        <link href="/assets/tabulator-master/tabulator-master/dist/css/tabulator_simple.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">


    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>

        <div class="container-fluid" id="wrapper" >
            <div class="row espaco"></div>
            <div class="row">
                <div class="col-sm-2 text-centert data">
                    Máquina:
                </div>
                <div class='col-sm-10'>
                    <select class="form-control" id="idMaquina">
                        <?php
                        foreach ($maquinas as $key => $maquina) {
                            if ($key == 0)
                                echo "<option selected value='{$maquina['id']}' >{$maquina['nome']}</option>";
                            else
                                echo "<option value='{$maquina['id']}' >{$maquina['nome']}</option >";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-centert">
                    <button id="buscarHistorico" type="button" class="btn btn-primary">Buscar</button>
                </div>
            </div>
            <div class="row">;
                <div class='col-sm-6'>
                    <h4>Histórico</h4>
                </div>
            </div>
            <div class="row">
                <div class='col-sm-12'>
                    <div id="historico_msg"></div>
                </div>
            </div>
        </div>
        <div id="historico_retrato" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 id="historico_data" class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <div id="historico_texto">
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script>


        </script>

        <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script async src="/assets/blockUI/jquery.blockUI.js"></script>
        <script src="/assets/jquery-ui-1.12.1/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/assets/tabulator-master/tabulator-master/dist/js/tabulator.min.js"></script>
        <script>
            $(document).ready(function () {

                $("#historico_msg").tabulator({
                    height: "100%",
                    layout: "fitColumns",
                    layoutColumnsOnNewData: true,
                    placeholder: "Sem dados",
                    pagination:"local", //enable local pagination.
                    paginationSize:10,
                    columns: [
                        {title: "Data", field: "datetime", formatter: "plaintext", headerFilter: "input",sorterParams: {format: "DD/MM/YYYY HH:mm"}},
                        {title: "Ver Itens", field: "modal", formatter: "html", align : "center"}
                    ]
                });
                $("#buscarHistorico").on("click", function () {

                    $("#historico_msg").tabulator("setData", "/ajax/ajax_checklist/historico_checklist_basico", {id_maquina: $("#idMaquina").val()}, "POST");
                    $("#historico_msg").tabulator("setSort", "datetime", "desc");
                });

                $(document).on("click", ".verRelatorio", function () {
                    $.ajax({
                        url:"/ajax/ajax_checklist/historico_checklist",
                        method:"POST",
                        dataType: "json",
                        data:{id_checklist: $(this).attr('id')},
                        success: function(resposta){
                            $("#historico_data").html(resposta[0].datetime);
                            var historico_texto = $("#historico_texto");
                            historico_texto.html("");
                            //Adicionando Maquina e data
                            //Adicionando items criticos
                            historico_texto.append("<h3>Items Críticos</h3>");
                            var itens_criticos = $( document.createElement('ul') );
                            $.each(resposta[0].itens_criticos_decoded,function(index, element){
                                var li = $( document.createElement('li') );
                                li.html("<b>" +index + ":</b> " + element);
                                itens_criticos.append(li);
                            });
                            historico_texto.append(itens_criticos);
                            //Adicionando itens gerais
                            historico_texto.append("<h3>Items Gerais</h3>");
                            var itens_gerais = $( document.createElement('ul') );
                            $.each(resposta[0].itens_gerais_decoded,function(index, element){
                                var li = $( document.createElement('li') );
                                li.html("<b>" +index + ":</b> " + element);
                                itens_gerais.append(li);
                            });
                            historico_texto.append(itens_gerais);
                            
                            $("#historico_retrato").modal('toggle');
                        }
                    })
                    
                });


            });
        </script>
    </body>



</html>
