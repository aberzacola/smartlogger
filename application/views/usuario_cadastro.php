<html>
    <head>
        <title>Condutores - Rastreio</title>
        <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script async src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script async src="/assets/blockUI/jquery.blockUI.js"></script>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/condutores.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div id="wrapper">
            <div id="cadastro_sucesso" class="alert alert-success hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Operador cadastrado com sucesso
            </div>
            <div id="operador_existente" class="alert alert-danger hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Operador já cadastrado
            </div>
            <div id="erro_desconhecido" class="alert alert-danger hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Erro ao cadastrar operador, entre em contato com o suporte
            </div>
            <form id="formulario_cadastro_condutor">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nome</label>
                    <input type="text" class="form-control" id="nome_condutor" name="nome_condutor" placeholder="Nome Condutor">
                </div>
                <button type="submit" class="btn btn-default">Cadastrar</button>
            </form>
        </div>
    </body>
    <script>

        $("#formulario_cadastro_condutor").on("submit", function (e) {
            e.preventDefault();
            $('#formulario_cadastro_condutor').block({
                message: '<h3>Castrando...</h3>'
            });
            $.ajax({
                method: "POST",
                url: "/ajax/ajax_condutores/cadastra_condutor",
                data: {nome: $("#nome_condutor").val(),id_empresa: <?=$id_empresa?>}
            }).done(function (msg_2) {
                if (msg_2 === "0") {
                    mostra_alerta("erro_desconhecido");
                    $('#formulario_cadastro_condutor').unblock();
                } else {
                    mostra_alerta("cadastro_sucesso");
                    $('#formulario_cadastro_condutor').unblock();
                    $("#formulario_cadastro_condutor").trigger("reset");
                }
            });

        });

        function mostra_alerta(alerta) {

            if (alerta === "cadastro_sucesso") {
                $("#cadastro_sucesso").removeClass("hidden");
                $("#operador_existente").addClass("hidden");
                $("#erro_desconhecido").addClass("hidden");

            } else if (alerta === "operador_existente") {
                $("#cadastro_sucesso").addClass("hidden");
                $("#operador_existente").removeClass("hidden");
                $("#erro_desconhecido").addClass("hidden");

            } else if (alerta === "erro_desconhecido") {
                $("#cadastro_sucesso").addClass("hidden");
                $("#operador_existente").addClass("hidden");
                $("#erro_desconhecido").removeClass("hidden");

            }
        }





    </script>



</html>
