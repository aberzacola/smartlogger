<html>
    <head>
        <title>Mapa - Cerca</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa_listar_cerca.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/simpleLittleTable.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>

            #map{
                width: 500px;
                height: 500px;
            }

        </style>


    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid" id="wrapper" >
            <div class="row espaco"></div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="cerca_removida_sucesso" class="alert alert-success hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        Cerca deletada com sucesso
                    </div>
                    <div id="cerca_removida_erro" class="alert alert-danger hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        Erro ao deletar a cerca
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-12" >
                    <table cellspacing='0'> <!-- cellspacing='0' is important, must stay -->
                        <tr><th>Nome Cerca</th><th>Ver Cerca</th><th>Deletar Cerca</th></tr><!-- Table Header -->                
                        <?php
                        foreach ($cercas as $key => $cerca) {
                            echo "<tr>"
                            . "<td><strong>{$cerca['nome_cerca']}</strong></td>"
                            . "<td><a href='#' ><img src='/assets/img/map-pointer2.png' alt='Ver cerca' class='ver_cerca' id_cerca='{$cerca['id']}' title='Ver cerca' /></a></td>"
                            . "<td><a href='#' ><img src='/assets/img/delete96.png' alt='Deletar cerca' class='deletar_cerca' id_cerca='{$cerca['id']}' title='Deletar cerca' /></a>"
                            . "</td>"
                            . "</tr>";
                        }
                        ?>

                    </table>
                </div>
            </div>

        </div>


        <!-- Modal -->
        <div class="modal fade" id="modal_mapa_cerca" tabindex="-1" role="dialog" aria-labelledby="modal_mapa_cerca_label">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modal_mapa_cerca_label">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div id="map"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>


    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google_maps_key") ?>&callback=initMap" async defer></script>
    <script >
        var map;
        var poly;
        var num_marks = 0;
        var marker = [];
        var iconBase = "http://monitor.injetec.com.br/assets/img/";
        $(".deletar_cerca").on("click", function (e) {

            var $estaCerca = $(this);
            var confirma = confirm("Realmente deseja deletar a cerca?");
            if (confirma === true) {
                $.ajax({
                    method: "POST",
                    url: "/ajax/ajax_cerca/deleta_cerca",
                    data: {id_cerca: $(this).attr("id_cerca")},
                    success: function () {
                        $("#cerca_removida_sucesso").removeClass("hidden");
                        $("#cerca_removida_erro").addClass("hidden");
                        $estaCerca.parents("tr").hide();
                    },
                    error: function () {
                        $("#cerca_removida_sucesso").addClass("hidden");
                        $("#cerca_removida_erro").removeClass("hidden");
                    }
                });
            }

        });

        $(".ver_cerca").on("click", function (e) {

            $.ajax({
                method: "POST",
                url: "/ajax/ajax_cerca/get_cerca",
                data: {id_cerca: $(this).attr('id_cerca')}
            }).done(function (msg) {
                for (i = 0; i < marker.length; i++) {
                    marker[i].setMap(null);
                }
                poly.setMap(null);
                var bounds = new google.maps.LatLngBounds();
                var calculo_centro = [];
                var caminho = [];
                var cerca = $.parseJSON(msg);
                var num_pontos = cerca['poligono'].length;
//                map.panTo({lat: parseFloat(poligono['poligono'][0][0]), lng: parseFloat(poligono['poligono'][0][1])});
                for (i = 0; i < num_pontos; i++) {
                    calculo_centro[i] = new google.maps.LatLng(parseFloat(cerca['poligono'][i][0]), parseFloat(cerca['poligono'][i][1]));
                    marker[num_marks] = new google.maps.Marker({
                        position: {lat: parseFloat(cerca['poligono'][i][0]), lng: parseFloat(cerca['poligono'][i][1])},
                        title: "#" + i,
                        icon: iconBase + 'fence1.png',
                        map: map
                    });
                    caminho[i] = {lat: parseFloat(cerca['poligono'][i][0]), lng: parseFloat(cerca['poligono'][i][1])};
                    num_marks++;
                }

                for (i = 0; i < num_pontos; i++) {
                    bounds.extend(calculo_centro[i]);
                }


                poly = new google.maps.Polyline({
                    path: caminho,
                    strokeColor: '#000000',
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                });

                poly.setMap(map);
                map.setCenter(bounds.getCenter());
                map.setZoom(parseInt(cerca['zoom']));


            });


            $('#modal_mapa_cerca').modal();

        });

        $('#modal_mapa_cerca').on('shown.bs.modal', function (e) {
            google.maps.event.trigger(map, "resize");
        })

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });

            poly = new google.maps.Polyline({
                strokeColor: '#000000',
                strokeOpacity: 1.0,
                strokeWeight: 3
            });
            poly.setMap(map);
        }








    </script>
</body>



</html>
