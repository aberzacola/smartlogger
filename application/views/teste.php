<link rel="stylesheet" type="text/javascript" href="../assets/jquery/js/jquery-2.1.4.min.js" />
<link rel="stylesheet" type="text/javascript" href="../assets/bootstrap/js/bootstrap.min.js" />
<link rel="stylesheet" type="text/css" href="../assets/bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css" href="../assets/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="../assets/css/painel.css" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<div id="tool-bar" >
    <ul>
        <li><a data-toogle="modal" data-target="#cadastro_condutor" ><img src="../../assets/img/constructor2.png" alt="Cadastrar condutor" title="Cadastrar condutor" ></a></li>
    </ul>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>