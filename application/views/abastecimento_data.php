<html>
    <head>
        <title>Alerta Lista</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa_rotas.css" />
        <link rel="stylesheet" type="text/css" href="/assets/datetimepicker/css/bootstrap-datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" href="/assets/bootstrap-table/bootstrap-table.css">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style>


        </style>

    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid" id="wrapper" >
            <div class="row">
                <div class="row espaco"></div>
                 <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Máquina:
                    </div>
                    <div class='col-sm-10'>
                        <select class="form-control" id="idMaquina">
                            <?php
                            foreach ($maquinas as $key => $maquina) {
                                if ($key == 0)
                                    echo "<option selected value='{$maquina['id']}' >{$maquina['nome']}</option>";
                                else
                                    echo "<option value='{$maquina['id']}' >{$maquina['nome']}</option >";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
                <div class="alert alert-info hidden" id="semData">
                    <strong>Selecione uma data</strong>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Data Inicial:
                    </div>
                    <div class='col-sm-10 data'>
                        <div class="form-group">
                            <div class='input-group date' id='dataInicial'>
                                <input type='text' class="form-control" placeholder="Clique no calendário ao lado"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Data Final:
                    </div>
                    <div class='col-sm-10 data'>
                        <div class="form-group">
                            <div class='input-group date' id='dataFinal'>
                                <input type='text' class="form-control" placeholder="Clique no calendário ao lado"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class='col-sm-12 data'>
                        <button id="tracar-rota" type="button" class="btn btn-primary pull-right">Ver Resultados</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="espaco">

        </div>
        <div class="container-fluid hidden" id="tabela_abastecimento">
            <div class="row" >
                <div class="col-sm-12">
                    <table id="table">
                        <thead>
                            <tr>
                                <th data-field="abastecimento_qtd">Qtd</th>
                                <th data-field="horimetro">Horimetro</th>
                                <th data-field="abastecimento_timestamp">Data</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/momentJs/moment.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/datetimepicker/js/bootstrap-datetimepicker.min.js" defer></script>
    <script src="/assets/bootstrap-table/bootstrap-table.js"></script>

    <script >

        $(document).ready(function () {

            $('#table').bootstrapTable({});


            var dataInicialSetada = false;
            var dataFinalSetada = false;

            $('#dataInicial').datetimepicker({
                format: 'DD/MM/YYYY HH:mm'
            });
            $('#dataFinal').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD/MM/YYYY HH:mm'
            });

            $("#dataInicial").on("dp.change", function (e) {
                $('#dataFinal').data("DateTimePicker").minDate(e.date);
                dataInicialSetada = true;

            });
            $("#dataFinal").on("dp.change", function (e) {
                $('#dataInicial').data("DateTimePicker").maxDate(e.date);
                dataFinalSetada = true;
            });

            $("#tracar-rota").on("click", function () {

                if (dataInicialSetada == false) {
                    $("#semData").removeClass("hidden");
                    $("#semData strong").html("Selecione a data Inicial");
                    return;
                }

                if (dataFinalSetada == false) {
                    $("#semData").removeClass("hidden");
                    $("#semData strong").html("Selecione a data Final");
                    return;
                }

                $("#semData").addClass("hidden");

                var dataFinal = $("#dataFinal").data("DateTimePicker").date().format('X');
                var dataInicial = $("#dataInicial").data("DateTimePicker").date().format('X');
                var idMaquina = $("#idMaquina").val();


                $.ajax({
                    url: "/ajax/ajax_abastecimento/abastecimento_maquina",
                    data: {id_maquina:idMaquina, data_inicio: dataInicial, data_final: dataFinal},
                    method: "POST",
                    dataType: "json",
                    success: function (resposta) {
                        console.log(resposta);
                        $("#tabela_abastecimento").removeClass("hidden");
                        $('#table').bootstrapTable('load', resposta);
                    }
                });


            });
        });
    </script>
</body>



</html>
