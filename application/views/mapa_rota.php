<html>
    <head>
        <title>Mapa - Rastreio</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">


    </head>
    <body>
        <div id="tool-bar" >
            <ul>
                <li><a href="/mapa/<?php echo $id_router_gsm; ?>"><img src="../../assets/img/map30.png" alt="Localização da máquina" title="Localização da máquina"></a></li>
            </ul>
        </div>
        <div class="content-fluid" id="wrapper" >
            <div id="map"></div>
        </div>
        <script>

            // This example creates a 2-pixel-wide red polyline showing the path of William
            // Kingsford Smith's first trans-Pacific flight between Oakland, CA, and
            // Brisbane, Australia.
            function initMap() {

                var inicio = {
                    path: 'M -2,0 0,-2 2,0 0,2 z',
                    strokeColor: '#292',
                    fillColor: '#292',
                    fillOpacity: 1
                };

                var fim = {
                    path: 'M -2,-2 2,2 M 2,-2 -2,2',
                    strokeColor: '#F00',
                    strokeWeight: 4
                };



                var seta = {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                    strokeColor: '#0000FF',
                    fillColor: '#0000FF',
                    fillOpacity: 1
                };

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 3,
                    center: {lat: 0, lng: -180},
                    mapTypeId: google.maps.MapTypeId.TERRAIN
                });

                var flightPlanCoordinates = [
                    {lat: 37.772, lng: -122.214},
                    {lat: 21.291, lng: -157.821}
                ];
                var flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    icons: [{
                            icon: inicio,
                            offset: '0%'
                        }, {
                            icon: seta,
                            offset: '100%'
                        }],
                    geodesic: true,
                    strokeColor: '#000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });

                var flightPlanCoordinates = [
                    {lat: 21.291, lng: -157.821},
                    {lat: -18.142, lng: 178.431}
                ];
                var flightPath2 = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    icons: [{
                            icon: fim,
                            offset: '100%'
                        }],
                    geodesic: true,
                    strokeColor: '#000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });

                flightPath.setMap(map);
                flightPath2.setMap(map);
            }

        </script>
       
        <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google_maps_key") ?>&callback=initMap" async defer></script>
    </body>



</html>
