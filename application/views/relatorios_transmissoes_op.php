<html>
    <head>
        <title>Relatório Transmissões OP</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa_rotas.css" />
        <link rel="stylesheet" type="text/css" href="/assets/datetimepicker/css/bootstrap-datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" href="/assets/bootstrap-table/bootstrap-table.css">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style>


        </style>

    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid" id="wrapper" >
            <div class="row">
                <div class="row espaco"></div>
                <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Operador:
                    </div>
                    <div class='col-sm-10'>
                        <select class="form-control" id="idOperador">
                            <?php
                            foreach ($operadores as $key => $operador) {
                                if ($key == 0)
                                    echo "<option selected value='{$operador['id']}' >{$operador['nome']}</option>";
                                else
                                    echo "<option value='{$operador['id']}' >{$operador['nome']}</option >";
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="alert alert-info hidden" id="semData">
                    <strong>Selecione uma data</strong>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Data Inicial:
                    </div>
                    <div class='col-sm-10 data'>
                        <div class="form-group">
                            <div class='input-group date' id='dataInicial'>
                                <input type='text' class="form-control" placeholder="Clique no calendário ao lado"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Data Final:
                    </div>
                    <div class='col-sm-10 data'>
                        <div class="form-group">
                            <div class='input-group date' id='dataFinal'>
                                <input type='text' class="form-control" placeholder="Clique no calendário ao lado"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class='col-sm-12 data'>
                        <button id="tracar-rota" type="button" class="btn btn-primary pull-right">Ver Resultados</button>
                    </div>
                </div>

            </div>
        </div>
    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/momentJs/moment.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/datetimepicker/js/bootstrap-datetimepicker.min.js" defer></script>

    <script >

        $(document).ready(function () {

            var dataInicialSetada = false;
            var dataFinalSetada = false;

            $('#dataInicial').datetimepicker({
                format: 'DD/MM/YYYY HH:mm'
            });
            $('#dataFinal').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD/MM/YYYY HH:mm'
            });

            $("#dataInicial").on("dp.change", function (e) {
                $('#dataFinal').data("DateTimePicker").minDate(e.date);
                dataInicialSetada = true;

            });
            $("#dataFinal").on("dp.change", function (e) {
                $('#dataInicial').data("DateTimePicker").maxDate(e.date);
                dataFinalSetada = true;
            });

            $("#tracar-rota").on("click", function () {
                
                 if (dataInicialSetada == false) {
                    $("#semData").removeClass("hidden");
                    $("#semData strong").html("Selecione a data Inicial");
                    return;
                }

                if (dataFinalSetada == false) {
                    $("#semData").removeClass("hidden");
                    $("#semData strong").html("Selecione a data Final");
                    return;
                }

                var dataFinal = $("#dataFinal").data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:00');
                var dataInicial = $("#dataInicial").data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:00');

                var dataFinalComparacao = $("#dataFinal").data("DateTimePicker").date().format('X');
                var dataInicialComparacao = $("#dataInicial").data("DateTimePicker").date().format('X');

                console.log(dataFinalComparacao,dataInicialComparacao)
                if (dataFinalComparacao <= dataInicialComparacao) {
                    $("#semData").removeClass("hidden");
                    $("#semData strong").html("Data final não pode ser maior que a inicial");
                    return;
                }
                $("#semData").addClass("hidden");
                
                var idOperador = $('#idOperador').find(":selected").val();
                
                window.location.href = "/relatorios/transmissoes_excell_op/"+idOperador+"/"+dataInicialComparacao+"/"+dataFinalComparacao;
            });
        });
    </script>
</body>



</html>
