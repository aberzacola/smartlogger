<html>
    <head>
        <title>Mapa - Configurações</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/posicao_padrao.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/simpleLittleTable.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid" id="wrapper" >
            <div id="map"></div>
        </div>
    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script>
        var map;
        var marker = [];
        var num_marker = 0;
        var existe_cadastro = 0;







        function initMap() {



<?php
if ($dados_empresa['lat_inicial'] == null) {
    echo "var myLatlng = {lat: -19.9178713, lng: -43.9603116};
          var zoom = 10;
          var ja_cadastrado = 0;";
} else {
    echo "var myLatlng = {lat: {$dados_empresa['lat_inicial']}, lng: {$dados_empresa['lng_inicial']}};
          var zoom = {$dados_empresa['zoom_inicial']}; var ja_cadastrado = 1;";
}
?>
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: zoom,
                center: myLatlng
            });

            if (ja_cadastrado === 1) {
                marker[num_marker] = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: 'Posição padrão'
                });
                num_marker++;
            }

            map.addListener('click', function (e) {

                if (num_marker > 0) {
                    marker[num_marker - 1].setMap(null);
                }

                // 3 seconds after the center of the map has changed, pan back to the
                // marker.
                marker[num_marker] = new google.maps.Marker({
                    position: e.latLng,
                    map: map,
                    title: 'Posição padrão'
                });
                num_marker++;
                map.setCenter(e.latLng);
                var confirma = confirm("Deseja salvar essa posição e zoom no mapa como Padrão?");
                if (confirma === true) {
                    $.ajax({
                        method: "POST",
                        url: "/ajax/ajax_configs/cadastra_ponto_padrao",
                        data: {id_empresa: <?php echo $id_empresa ?>, lat: e.latLng.lat(), lng: e.latLng.lng(), zoom: map.getZoom()}
                    }).done(function (msg) {
                        alert("Posição padrão e zoom alterados com sucesso");
                    });

                }





            });
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?=$this->config->item("google_maps_key")?>&signed_in=true&callback=initMap" async defer>
    </script>

</body>



</html>
