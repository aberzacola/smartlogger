<html>
    <head>
        <title>Condutores - Rastreio</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/condutores.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div id="wrapper">
            <div id="cadastro_sucesso" class="alert alert-success hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Empresa deletadas com sucesso
            </div>
            <div id="erro_desconhecido" class="alert alert-danger hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Erro desconhecido
            </div>
            <form id="formulario_deletar_empresa">
                <div class='row'>
                    <div class='col-lg-12'>
                        <label for="id_empresa">Escolha a empresa</label>
                        <select class="form-control" name='id_empresa' id="id_empresa">
                            <?php
                            echo "<option value='0' >Escolha uma empresa</option>";
                            foreach ($empresas as $empresa) {
                                $selected = "";
                                if ($empresa == $empresa['id']) {
                                    $selected = "selected";
                                }
                                echo "<option value='{$empresa['id']}' $selected>{$empresa['nome']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="espaco" ></div>
                <button type="submit" class="btn btn-default">Deletar</button>
            </form>
        </div>
    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script async src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script async src="/assets/blockUI/jquery.blockUI.js"></script>
    <script>


        $("#formulario_deletar_empresa").on("submit", function (e) {
            e.preventDefault();

            if($('#id_empresa').find(":selected").val() == "0"){
                alert("Selecione uma empresa");
                return;
            }
            
            
            $('#formulario_deletar_empresa').block({
                message: '<h3>Verificando...</h3>'
            });
            
            
            
            $.ajax({
                method: "POST",
                url: "/admin/ajax/ajax_configs/deleta_empresa",
                data: {id_empresa: $('#id_empresa').find(":selected").val()},
                dataType: "json",
                success: function (resposta) {
                    $('#formulario_deletar_empresa').unblock();
                    if (resposta) {
                        mostra_alerta("deletado_sucesso");
                    } else {
                        cadastra_usuario();
                    }
                }
            });
        });


        function mostra_alerta(alerta) {

            if (alerta === "deletado_sucesso") {
                $("#cadastro_sucesso").removeClass("hidden");
                $("#usuario_existente").addClass("hidden");
                $("#erro_desconhecido").addClass("hidden");

            } else if (alerta === "erro_desconhecido") {
                $("#cadastro_sucesso").addClass("hidden");
                $("#usuario_existente").addClass("hidden");
                $("#erro_desconhecido").removeClass("hidden")
            }
        }



    </script>



</html>
