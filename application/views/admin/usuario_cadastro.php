<html>
    <head>
        <title>Condutores - Rastreio</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/condutores.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div id="wrapper" class="container-fluid">
            <div id="cadastro_sucesso" class="alert alert-success hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Usuário cadastrado com sucesso
            </div>
            <div id="usuario_existente" class="alert alert-danger hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Email já cadastrado
            </div>
            <div id="erro_desconhecido" class="alert alert-danger hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Erro ao cadsatrar usuário
            </div>
            <form id="formulario_cadastro_usuario">
                <div class='row'>
                    <div class='col-lg-12'>
                        <label for="id_empresa">Escolha a empresa</label>
                        <select class="form-control" name='id_empresa' id="id_empresa">
                            <?php
                            echo "<option value='0' >Escolha uma empresa</option>";
                            foreach ($empresas as $empresa) {
                                $selected = "";
                                if ($empresa == $empresa['id']) {
                                    $selected = "selected";
                                }
                                echo "<option value='{$empresa['id']}' $selected>{$empresa['nome']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nome</label>
                    <input type="text" class="form-control" id="nome_usuario" name="nome_usuário" placeholder="Nome Condutor" required="true">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" id="email" name="email_usuario" placeholder="Email" required="true">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Senha</label>
                    <input type="text" class="form-control" id="senha" name="email_usuario" placeholder="Senha" required="true">
                </div>
                <button type="submit" class="btn btn-default">Cadastrar</button>
            </form>
        </div>
    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script async src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script async src="/assets/blockUI/jquery.blockUI.js"></script>
    <script>


        $("#formulario_cadastro_usuario").on("submit", function (e) {
            e.preventDefault();

            if($('#id_empresa').find(":selected").val() == "0"){
                alert("Selecione uma empresa");
                return;
            }
            
            
            $('#formulario_cadastro_usuario').block({
                message: '<h3>Verificando...</h3>'
            });
            
            
            
            $.ajax({
                method: "POST",
                url: "/admin/ajax/ajax_configs/check_existencia_email",
                data: {email: $("#email").val()},
                dataType: "json",
                success: function (resposta) {
                    $('#formulario_cadastro_usuario').unblock();
                    if (resposta) {
                        mostra_alerta("operador_existente");
                    } else {
                        cadastra_usuario();
                    }
                }
            });
        });

        function cadastra_usuario() {
            $('#formulario_cadastro_usuario').block({
                message: '<h3>Cadastrando usuario...</h3>'
            });
            $.ajax({
                method: "POST",
                url: "/admin/ajax/ajax_configs/cadastra_usuario_empresa",
                data: {
                    email: $("#email").val(), 
                    nome: $("#nome_usuario").val(), 
                    senha: $("#senha").val(),
                    id_empresa: $('#id_empresa').find(":selected").val()},
                dataType: "json",
                success: function (resposta) {
                    $('#formulario_cadastro_usuario').unblock();
                    mostra_alerta("cadastro_sucesso");
                    $('#formulario_cadastro_usuario')[0].reset();
                },
                error: function () {
                    $('#formulario_cadastro_usuario').unblock();
                    mostra_alerta("erro_desconhecido");
                }
            })
        }


        function mostra_alerta(alerta) {

            if (alerta === "cadastro_sucesso") {
                $("#cadastro_sucesso").removeClass("hidden");
                $("#usuario_existente").addClass("hidden");
                $("#erro_desconhecido").addClass("hidden");

            } else if (alerta === "operador_existente") {
                $("#cadastro_sucesso").addClass("hidden");
                $("#usuario_existente").removeClass("hidden");
                $("#erro_desconhecido").addClass("hidden");

            } else if (alerta === "erro_desconhecido") {
                $("#cadastro_sucesso").addClass("hidden");
                $("#usuario_existente").addClass("hidden");
                $("#erro_desconhecido").removeClass("hidden")
            }
        }



    </script>



</html>
