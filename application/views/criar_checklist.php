<html>
    <head>
        <title>Checklist - Monitor</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/criar_checklist.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">


    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>

        <div class="container-fluid" id="wrapper" >
            <div class="row espaco"></div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="cadastro_sucesso" class="alert alert-success hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        Checklist Atualizado
                    </div>
                    <div id="cadastro_errado" class="alert alert-danger hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        Erro ao atualizar o checklist
                    </div>
                    <div class="alert alert-info">
                        <strong>Digite um item por linha</strong>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form id="form_checklist" class="form-horizontal">
                        <div class="form-group">
                            <label for="itens_criticos" class="col-sm-2 control-label">Itens Críticos</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="itens_criticos" rows="6"><?=$checklist['itens_criticos']?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="itens_gerais" class="col-sm-2 control-label">Itens Gerais</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="itens_gerais" rows="6"><?=$checklist['itens_gerais']?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Atualizar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>


        </script>

        <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script async src="/assets/blockUI/jquery.blockUI.js"></script>
        <script>
            $(document).ready(function () {

                $("#form_checklist").on("submit", function (e) {
                    e.preventDefault();

                    $('#form_checklist').block({
                        message: '<h3>Castrando...</h3>'
                    });

                    $.ajax({
                        url: "/ajax/ajax_checklist/cadastra_check_list",
                        method: "POST",
                        dataType: "json",
                        data: {itens_criticos: $("#itens_criticos").val(), itens_gerais: $("#itens_gerais").val()},
                        success: function (resposta) {
                             $('#form_checklist').unblock();
                            console.log(resposta)
                            if(resposta == 1){
                                $("#cadastro_sucesso").removeClass("hidden");
                                $("#cadastro_errado").addClass("hidden");
                            }else{
                                $("#cadastro_sucesso").addClass("hidden");
                                $("#cadastro_errado").removeClass("hidden");
                            }
                        }
                    });
                });

            });
        </script>
    </body>



</html>
