<?php

class Rotas_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_intervalo_id_trans($id_trans_inicio,$id_trans_final){
        
        $this->db->where("id >=" ,$id_trans_inicio);
        $this->db->where("id <=" ,$id_trans_final);
        $this->db->select("lng");
        $this->db->select("lat");
        $resposta = $this->db->get("tbl_transmissao");
        return $resposta->result_array();
        
    }
    
    function get_intervalo_datas_trans($data_inicial,$data_final,$id_maquina){
        $this->db->where("timestamp_transmissao >=" ,$data_inicial);
        $this->db->where("timestamp_transmissao <=" ,$data_final);
        $this->db->where("id_maquina" ,$id_maquina);
        $this->db->select("lng");
        $this->db->select("lat");
        $resposta = $this->db->get("tbl_transmissao");
        return $resposta->result_array();
    }

}
