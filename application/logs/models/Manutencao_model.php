<?php

class Manutencao_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function set_peca_nova($id_empresa,$id_maquina,$nome,$id_trans_horimetro_inicial,$tempo_utilizado,$tempo_limite,$obs) {
        
        $this->db->insert("tbl_manutencao", array(
            "id_empresa" => $id_empresa,
            "id_maquina" => $id_maquina,
            "nome" => $nome,
            "obs" => $obs,
            "id_trans_horiemtro_inicial" => $id_trans_horimetro_inicial,
            "tempo_utilizacao" => $tempo_utilizado,
            "tempo_limite" => $tempo_limite
        ));

        return $this->db->insert_id();
    }
    
    //Pega infos das pecas de uma maquina
    function get_pecas_maquina($id_maquina){
        
        $this->db->where("id_maquina", $id_maquina);
        $resultado = $this->db->get("tbl_manutencao");
        
        return $resultado->result_array();
    
    }

}
