<?php

class Maquina_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_last_on_off_maquina($id_maquina) {
        $this->db->limit(1);
        $this->db->where('id_maquina', $id_maquina);
        $this->db->order_by('data_hora', 'DESC');
        $query = $this->db->get('tbl_on_off');

        $resultado = $query->result_array();

        return current($resultado);
    }

    function get_last_alertas_cercas($id_maquina){
        
        $this->db->where('id_maquina', $id_maquina);
        $query = $this->db->get('tbl_alertas_cerca');

        $resultado = $query->result_array();
        
        return $resultado;
        
    }
    function get_last_transmission($id_maquina) {
        $this->db->limit(1);
        $this->db->where('id_maquina', $id_maquina);
        $this->db->order_by('timestamp_transmissao', 'DESC');
        $query = $this->db->get('tbl_transmissao');

        $resultado = $query->result_array();

        return $resultado;
    }

    function get_last_gps_valid_transmission($id_maquina) {
        $this->db->limit(1);
        $this->db->where('id_maquina', $id_maquina);
        $this->db->where('lat !=', 0);
        $this->db->order_by('timestamp_transmissao', 'DESC');
        $query = $this->db->get('tbl_transmissao');

        $resultado = $query->result_array();

        return $resultado;
    }

    // Pega todas as máquinas, cadastradas no sistema antigo
    function get_all_maquinas($id_empresa) {


        $this->db->where("id_empresa", $id_empresa);
        $resultado = $this->db->get("tbl_maquina");
        return $resultado->result_array();
    }

    // Pega todas as máquinas ativas, cadastradas no sistema antigo
    function get_all_maquinas_ativas($id_empresa) {


        $this->db->where("id_empresa", $id_empresa);
        $this->db->where("ativo", 1);
        $this->db->select("nome");
        $this->db->select("id");
        $resultado = $this->db->get("tbl_maquina");
        return $resultado->result_array();
    }

    //Cadastra nova máquina
    function cadastra_maquina($id_empresa, 
            $nome_maquina, 
            $senha, 
            $horas_horizonte,
            $setPointTemperatura,
            $setPointImpacto,
            $setPointVelocidade, 
            $tipoCombustivel,
            $setPointTimeout) {

        $this->db->insert("tbl_maquina", array(
            "id_empresa" => $id_empresa,
            "nome" => $nome_maquina,
            "senha" => $senha,
            "horas_horizonte" => $horas_horizonte,
            "set_point_temperatura" => $setPointTemperatura,
            "set_point_impacto" => $setPointImpacto,
            "set_point_velocidade" => $setPointVelocidade,
            "tipo_combustivel" => $tipoCombustivel,
            "set_point_conexao_minutos" => $setPointTimeout
        ));

        return $this->db->insert_id();
    }

    //Deleta máquina
    public function deleta_maquina($id_maquina) {

        $this->db->where("id", $id_maquina);
        return $this->db->delete("tbl_maquina");
    }

    function atualiza_maquina($id_maquina, $nome_maquina, $horas_horizonte,
            $setPointTemperatura,
            $setPointImpacto,
            $setPointVelocidade, 
            $tipoCombustivel,
            $setPointTimeout) {

        $data = array(
            'nome' => $nome_maquina,
            'horas_horizonte' => $horas_horizonte,
            "set_point_temperatura" => $setPointTemperatura,
            "set_point_impacto" => $setPointImpacto,
            "set_point_velocidade" => $setPointVelocidade,
            "tipo_combustivel" => $tipoCombustivel,
            "set_point_conexao_minutos" => $setPointTimeout
        );
        $this->db->where('id', $id_maquina);
        $this->db->update('tbl_maquina', $data);
    }

    //Pega infos da maquina
    function get_maquina($id_maquina) {
        $this->db->where("id", $id_maquina);
        $resultado = $this->db->get("tbl_maquina");

        return current($resultado->result_array());
    }

    //Pega as transmissões a partir da data dada_inicio como parâmtro até a data de hoje
    public function get_odd_trans_since($id_maquina, $data_inicio) {

        //Essa query pega todos as trans
        //1$resultado = $this->db->query("SELECT * FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio')");
        //Essa query pega todas as trnas impares
        $resultado = $this->db->query("SELECT * FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB WHERE MOD(ID,2) = 1");
        return $resultado->result_array();
    }
    
    //Pega as transmissões a partir da data dada_inicio como parâmtro até a data final
    public function get_all_trans_between($id_maquina, $data_inicio, $data_final) {

        //Essa query pega todos as trans
        $resultado = $this->db->query("SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio' AND timestamp_transmissao < '$data_final'");
        return $resultado->result_array();
    }

    //Pega as transmissões a partir da data dada_inicio como parâmtro até a data de hoje
    public function get_horimetro_since($id_maquina, $data_inicio) {

        //Essa query pega todos as trans
        $resultado = $this->db->query("SELECT horimetro,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB");

        //Essa query pega todas as trnas impares
//        $resultado = $this->db->query("SELECT horimetro,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB WHERE MOD(ID,2) = 1");
        return $resultado->result_array();
    }

    //Pega as transmissões a partir da data dada_inicio como parâmtro até a data de hoje
    public function get_hodometro_parcial_since($id_maquina, $data_inicio) {

        //Essa query pega todos as trans
        $resultado = $this->db->query("SELECT hodometro_parcial,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB");

        //Essa query pega todas as trnas impares
//        $resultado = $this->db->query("SELECT horimetro,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB WHERE MOD(ID,2) = 1");
        return $resultado->result_array();
    }

    //Pega as transmissões a partir da data dada_inicio como parâmtro até a data de hoje
    public function get_hora_efetiva_since($id_maquina, $data_inicio) {

        //Essa query pega todos as trans
        $resultado = $this->db->query("SELECT hora_efetiva,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB");

        //Essa query pega todas as trnas impares
//        $resultado = $this->db->query("SELECT horimetro,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB WHERE MOD(ID,2) = 1");
        return $resultado->result_array();
    }

    //Pega as transmissões a partir da data dada_inicio como parâmtro até a data de hoje
    public function get_acelerometro_since($id_maquina, $data_inicio) {

        //Essa query pega todos as trans
        $resultado = $this->db->query("SELECT acelerometro,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB");

        //Essa query pega todas as trnas impares
//        $resultado = $this->db->query("SELECT horimetro,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB WHERE MOD(ID,2) = 1");
        return $resultado->result_array();
    }

    //Pega as transmissões a partir da data dada_inicio como parâmtro até a data de hoje
    public function get_velocidade_since($id_maquina, $data_inicio) {

        //Essa query pega todos as trans
        $resultado = $this->db->query("SELECT velocidade,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB");

        //Essa query pega todas as trnas impares
//        $resultado = $this->db->query("SELECT horimetro,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB WHERE MOD(ID,2) = 1");
        return $resultado->result_array();
    }

    //Pega as transmissões a partir da data dada_inicio como parâmtro até a data de hoje
    public function get_temperatura_since($id_maquina, $data_inicio) {

        //Essa query pega todos as trans
        $resultado = $this->db->query("SELECT temperatura_pico,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB");

        //Essa query pega todas as trnas impares
//        $resultado = $this->db->query("SELECT horimetro,timestamp_transmissao as data FROM (SELECT * FROM tbl_transmissao WHERE id_maquina = $id_maquina  AND timestamp_transmissao > '$data_inicio') as TAB WHERE MOD(ID,2) = 1");
        return $resultado->result_array();
    }
    
    public function get_last_cinto($id_maquina){
        
        $this->db->where("id_maquina", $id_maquina);
        $this->db->order_by('data_hora', 'DESC');
        $this->db->limit(1);
        $resultado = $this->db->get("tbl_cinto_on_off");
        return $resultado->result_array();
        
    }

    
}
