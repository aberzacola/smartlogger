<?php

class Alertas_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function insert_alerta($dados) {
        $this->db->insert('tbl_alertas_cerca', $dados);
    }

    public function ultimo_alerta($id_maquina,$id_cerca) {

        $this->db->where('id_maquina', $id_maquina);
        $this->db->where('id_cerca', $id_cerca);
        $this->db->limit(1);
        $this->db->order_by('id', 'DESC');
        $resource = $this->db->get("tbl_alertas_cerca");
        $resultado = $resource->result_array();

        return (!empty($resultado)) ? $resultado[0] : false;
    }

    public function get_todos_alertas_maquina($id_maquina) {

        $this->db->where('id_maquina', $id_maquina);
        $resultado = $this->db->get('tbl_alertas_cerca');
        return $resultado->result_array();
    }
    public function get_todos_alertas_maquina_between($id_maquina,$id_transmissao_inicio,$id_transmissao_final) {

        $this->db->where('id_maquina', $id_maquina);
        $this->db->where('id_transmissao >=', $id_transmissao_inicio);
        $this->db->where('id_transmissao <=', $id_transmissao_final);
        $resultado = $this->db->get('tbl_alertas_cerca');
        return $resultado->result_array();
    }

    public function get_last_two_alerts($id_router_gsm) {
        $this->db->where('id_router_gsm', $id_router_gsm);
        $this->db->limit(4); // tem que ser multiplo de dois, por que cada alerta consistem em 2 entradas

        $resultado = $this->db->get('tbl_alertas_cerca');
        return $resultado->result_array();
    }

}
