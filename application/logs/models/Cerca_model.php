<?php

class Cerca_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //cadastrado o nome da cerca no banco
    public function set_cerca($nome, $id_empresa, $poligono, $zoom) {

        $string_poligono = "";
        foreach ($poligono as $coordenada) {
            $string_poligono .= "$coordenada[0] $coordenada[1], ";
        }
        $string_poligono .= "{$poligono[0][0]} {$poligono[0][1]}";


        $query = "INSERT INTO tbl_cercas(id,id_empresa  ,nome_cerca,poligono,zoom) VALUES (NULL,$id_empresa,'$nome',PolygonFromText('POLYGON(($string_poligono))') , $zoom)";

        $resposta = $this->db->query($query);

        return $resposta;
    }

    public function get_cercas($id_empresa) {

        $this->db->where("id_empresa", $id_empresa);
        $resposta = $this->db->get("tbl_cercas");

        return $resposta->result_array();
    }

    public function get_cerca($id_cerca) {

        $query = "SELECT AsText(poligono) as poligono,zoom FROM tbl_cercas WHERE id = $id_cerca";

        $resposta = $this->db->query($query);


        return current($resposta->result_array());
    }

    public function get_cerca_nome($id_cerca) {

        $this->db->where("id", $id_cerca);
        $this->db->select("nome_cerca");
        $resposta = $this->db->get("tbl_cercas");

        return current($resposta->result_array());
    }

    public function get_grupo_nome_cerca($ids_cerca) {
        $this->db->where_in("id", $ids_cerca);
        $this->db->select("id,nome_cerca");
        $resposta = $this->db->get("tbl_cercas");

        return $resposta->result_array();
    }

    public function deleta_cerca($id_cerca) {

        $this->db->where("id", $id_cerca);
        return $this->db->delete("tbl_cercas");
    }

}
