<?php

class Abastecimento_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_intervalo_id_trans($id_trans_inicio,$id_trans_final){
        
        $this->db->where("id >=" ,$id_trans_inicio);
        $this->db->where("id <=" ,$id_trans_final);
        $this->db->select("lng");
        $this->db->select("lat");
        $resposta = $this->db->get("tbl_transmissao");
        return $resposta->result_array();
        
    }
    
    /*function get_abastecimentos_unica_maquina($data_inicial,$data_final,$id_maquina){
        $resposta = $this->db->query(
                "SELECT id,horimetro,abastecimento_qtd,DATE_FORMAT(FROM_UNIXTIME(abastecimento_timestamp),'%d-%m-%Y %H:%i') as abastecimento_timestamp FROM
                (SELECT abastecimento_timestamp,MAX(tbl_transmissao.abastecimento_qtd) abastecimento_qtd,id,horimetro 
                FROM tbl_transmissao WHERE id_maquina = '$id_maquina' AND 
                abastecimento_timestamp <= '1523286478' AND
                abastecimento_timestamp >= '1522594377'
                AND abastecimento_timestamp != -1
                GROUP BY tbl_transmissao.abastecimento_qtd) A
                ORDER BY id DESC");
        
        return $resposta->result_array();
        
    }*/
    function get_abastecimentos_unica_maquina($data_inicial,$data_final,$id_maquina){
        $resposta = $this->db->query(
                "SELECT id,horimetro,abastecimento_qtd,
                DATE_FORMAT(FROM_UNIXTIME(abastecimento_timestamp),'%d-%m-%Y %H:%i') as abastecimento_timestamp
                FROM tbl_transmissao
                WHERE id_maquina = '$id_maquina' AND 
                abastecimento_timestamp <= '$data_final' AND
                abastecimento_timestamp >= '$data_inicial'
                AND abastecimento_timestamp != -1
                GROUP BY abastecimento_qtd
                ORDER BY id DESC");
        
        return $resposta->result_array();
        
    }
    
    function get_intervalo_datas_trans_multi_id($data_inicial,$data_final,$ids_maquinas){
        $this->db->where("timestamp_transmissao >=" ,$data_inicial);
        $this->db->where("timestamp_transmissao <=" ,$data_final);
//        $this->db->where("abastecimento_qtd !=" ,0);
        $this->db->where_in("id_maquina" ,$ids_maquinas);
        $this->db->select("unix_timestamp(timestamp_transmissao) as unix_time");
        $this->db->select("id_maquina");
        $this->db->select("abastecimento_qtd");
        $this->db->select("abastecimento_timestamp");
        $this->db->select("horimetro");
              
        $resposta = $this->db->get("tbl_transmissao");
        return $resposta->result_array();
    }

}
