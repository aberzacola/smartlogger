<?php

class Empresa_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_empresa($id_empresa) {
        $this->db->where('id', $id_empresa);
        $query = $this->db->get('tbl_empresas');

        $resultado = $query->result_array();

        return $resultado[0];
    }

}
