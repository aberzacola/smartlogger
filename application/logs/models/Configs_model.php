<?php

class Configs_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_cadastro_empresa($id_empresa) {

        $this->db->where("id", $id_empresa);
        $query = $this->db->get("tbl_empresas");

        return $query->result_array();
    }

    // cadastra só o ID antigo e nome...
    public function set_cadastro_basico_empresa($id_empresa, $nome) {

        $data = array("id_sistema_antigo" => $id_empresa,
            "nome" => $nome);

        $this->db->insert("tbl_empresas", $data);
    }

    public function set_ponto_padrao($id_empresa, $lat, $lng, $zoom) {


        $this->db->set("lat_inicial", $lat);
        $this->db->set("lng_inicial", $lng);
        $this->db->set("zoom_inicial", $zoom);
        $this->db->where("id", $id_empresa);
        $this->db->update("tbl_empresas");
    }

    public function get_nome_empresa_sis_antigo($id_empresa) {

        $sistema_antigo = $this->load->database("sistema_antigo", TRUE);

        $sistema_antigo->where("empId", $id_empresa);
        $sistema_antigo->select("empNome");
        $resposta = $sistema_antigo->get("empresa");

        $var = $resposta->result_array();
        return $var[0]['empNome'];
    }

    public function get_cadastro_emp_sistema_antigo($id_empresa_antigo) {
        $sistema_antigo = $this->load->database("sistema_antigo", TRUE);

        $sistema_antigo->where("empId", $id_empresa_antigo);
        $resposta = $sistema_antigo->get("empresa");

        $var = $resposta->result_array();
        return $var[0];
    }

    //Verifica se o email existe
    public function does_this_emails_exists($email) {
        $this->db->where("email", $email);
        $query = $this->db->get("tbl_usuarios");

        return $query->num_rows() == 0 ? FALSE : TRUE;
    }

    //cadastra um usuario
    public function cadastra_usuario($id_empresa, $nome, $email, $senha) {

        $this->db->insert("tbl_usuarios", array(
            "id_empresa" => $id_empresa,
            "nome" => $nome,
            "email" => $email,
            "senha" => $senha
        ));

        return $this->db->insert_id();
    }

    // Volta a lista de usuários cadastrados
    public function usuarios_cadastrados($id_empresa) {
        $this->db->where("id_empresa", $id_empresa);
        $query = $this->db->get("tbl_usuarios");

        return $query->result_array();
    }

    //deleta usuario
    public function deleta_usuario($id_usuario) {

        $this->db->where("id", $id_usuario);
        return $this->db->delete("tbl_usuarios");
    }

}
