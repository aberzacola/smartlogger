<?php

class Dados_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //cadastrado o nome da cerca no banco
    public function input_data($insert_uso_dados) {

        $this->db->insert_batch("tbl_uso_dados", $insert_uso_dados);
    }

    public function get_dados_entre_trans($id_maquina,$data_inicial,$data_final) {

        $this->db->where("id_maquina", $id_maquina);
        $this->db->where("data >=", $data_inicial);
        $this->db->where("data <=", $data_final);
        $resposta = $this->db->get("tbl_uso_dados");

        return $resposta->result_array();
    }

}
