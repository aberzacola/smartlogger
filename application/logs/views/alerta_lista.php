<html>
    <head>
        <title>Alerta Lista</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa_listar_cerca.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" type="text/css" href="/assets/datetimepicker/css/bootstrap-datetimepicker.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style>

            #map{
                width: 500px;
                height: 500px;
            }



            #legend {
                font-family: Arial, sans-serif;
                background: #fff;
                padding: 10px;
                margin: 10px;
                border: 3px solid #000;
            }
            #legend h5 {
                margin-top: 0;
            }
            #legend img {
                vertical-align: middle;
            }
            .plotar_mapa{
                cursor:pointer;
            }

            table.tbl_alertas th{
                border: 1px solid black;
            }
            table.tbl_alertas td{
                border: 1px solid black;
            }

            table.tbl_alertas th{
                padding: 15px;
            }
            table.tbl_alertas td{
                padding: 15px;
            }

        </style>

    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid" id="wrapper" >
            <form id="alertas-cerca" action="/alertas/alertas_cerca_lista/" method="POST">
                <div class="row">
                    <div class="row espaco"></div>
                    <div class="alert alert-info hidden" id="error">
                        <strong>Selecione uma data</strong>
                    </div>
                    <div class='row'>
                        <div class='col-lg-12'>
                            <label for="nome_maquina">Escolha a máquina </label>
                            <select class="form-control" name='id_maquina' id="id_maquina">
                                <?php
                                echo "<option value='0' >Escolha uma máquina</option>";
                                foreach ($maquinas as $maquina) {
                                    $selected = "";
                                    if ($id_maquina == $maquina['id']) {
                                        $selected = "selected";
                                    }
                                    echo "<option value='{$maquina['id']}' $selected>{$maquina['nome']}</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row espaco"></div>
                    <div class="alert alert-info hidden" id="semData">
                        <strong>Selecione uma data</strong>
                    </div>
                    <div class="row espaco"></div>
                    <div class="row">
                        <div class="col-sm-2 text-centert data">
                            Data Inicial:
                        </div>
                        <div class='col-sm-10 data'>
                            <div class="form-group">
                                <div class='input-group date' id='dataInicial'>
                                    <input type='text' name='dataInicial' class="form-control" placeholder="Clique no calendário ao lado"/>
                                    <input type='hidden' name='dataInicialSistema' class="form-control" placeholder="Clique no calendário ao lado"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row espaco"></div>
                    <div class="row">
                        <div class="col-sm-2 text-centert data">
                            Data Final:
                        </div>
                        <div class='col-sm-10 data'>
                            <div class="form-group">
                                <div class='input-group date' id='dataFinal'>
                                    <input type='text' name='dataFinal' class="form-control" placeholder="Clique no calendário ao lado"/>
                                    <input type='hidden' name='dataFinalSistema' class="fdorm-control" placeholder="Clique no calendário ao lado"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row espaco"></div>
                    <div class="row">
                        <div class='col-sm-12 data'>
                            <button type="submit" class="btn btn-primary pull-right">Filtrar</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
        <?php if(isset($nomeMaquina)):?>
        <div class="container-fluid" id="wrapper" >
            <div class="row" >
                <div class='col-lg-12'>
                    <h3>Maquina: <?=$nomeMaquina?></h3>
                    <h3>Periodo: <?=$dataInicio?> ate <?=$dataFinal?></h3>

                </div>
            </div>
        </div>
        <?php endif;?>
        <div class="container-fluid" id="wrapper" >
            <table class='tbl_alertas' id="tbl_alertas"> <!-- cellspacing='0' is important, must stay -->
                <tr>
                    <th>Máquina</th>
                    <th>Evento</th>
                    <th>Hora</th>
                    <th>Nome Condutor</th>
                    <th>Nome Cerca</th>
                    <th>Local</th>
                </tr><!-- Table Header -->
                <?php
                if ($todos_alertas) {
                    foreach ($todos_alertas as $alertas) {
                        echo "<tr>";
                        echo "<td>{$alertas['nome_maquina']}</td>";
                        echo "<td>{$alertas['tipo_evento']}</td>";
                        echo "<td>{$alertas['hora_evento']}</td>";
                        echo "<td>{$alertas['nome_condutor']}</td>";
                        echo "<td>{$alertas['nome_cerca']}</td>";
                        echo "<td><a href='#' ><img src='/assets/img/map-pointer2.png' alt='Ver Local' class='ver_local' id_cerca='{$alertas['id_cerca']}' lat='{$alertas['lat']}' lng='{$alertas['lng']}' title='Ver Local' /></a></td>";
                        echo "</tr>";
                    }
                }
                ?>
            </table>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="modal_local_cerca" tabindex="-1" role="dialog" aria-labelledby="modal_mapa_cerca_label">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modal_mapa_cerca_label">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div id="map">

                        </div>
                    </div>
                    <div class="hidden">
                        <div id="legend">
                            <h5>Legenda</h5>
                            <img width="15" src="/assets/img/inicio.png" /> Partida
                            <img width="15" src="/assets/img/direction.png" /> Direção
                            <img width="15" src="/assets/img/final.png" /> Fim
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>



    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/momentJs/moment.min.js"></script>
    <script src="/assets/datetimepicker/js/bootstrap-datetimepicker.min.js" defer></script>



    <script>
        var map;
        var poly;
        var num_marks = 0;
        var marker = [];
        var ponto_obstrucao = false;
        var iconBase = "http://smartlogger.injetec.com.br/assets/img/";

        $(document).ready(function () {

            var dataInicialSetada = false;
            var dataFinalSetada = false;
            var idMaquina = false;

            $('#dataInicial').datetimepicker({
                format: 'DD/MM/YYYY HH:mm'
            });
            $('#dataFinal').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD/MM/YYYY HH:mm'
            });

            $("#dataInicial").on("dp.change", function (e) {
                $('#dataFinal').data("DateTimePicker").minDate(e.date);
                dataInicialSetada = true;

            });
            $("#dataFinal").on("dp.change", function (e) {
                $('#dataInicial').data("DateTimePicker").maxDate(e.date);
                dataFinalSetada = true;
            });

            $("#alertas-cerca").on("submit", function (e) {
                e.preventDefault()
                var dataFinal = $("#dataFinal").data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:00');
                var dataInicial = $("#dataInicial").data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:00');
                
                $('[name=dataInicialSistema]').val(dataInicial);
                $('[name=dataFinalSistema]').val(dataFinal);
                
                idMaquina = $("#id_maquina").val();
                if (idMaquina == 0) {
                    $("#error").removeClass("hidden");
                    $("#error strong").html("Selecione a uma maquina");
                    return;
                }

                if (dataInicialSetada == false) {
                    $("#error").removeClass("hidden");
                    $("#error strong").html("Selecione a data Inicial");
                    return;
                }

                if (dataFinalSetada == false) {
                    $("#error").removeClass("hidden");
                    $("#error strong").html("Selecione a data Final");
                    return;
                }

                $("#semData").addClass("hidden");
                
                this.submit();

            });
        });

        $(".ver_local").on("click", function (e) {
            var lat_fora_cerca = parseFloat($(this).attr('lat'));
            var lng_fora_cerca = parseFloat($(this).attr('lng'));
            $.ajax({
                method: "POST",
                url: "/ajax/ajax_cerca/get_cerca",
                data: {id_cerca: $(this).attr('id_cerca')}
            }).done(function (msg) {
                for (i = 0; i < marker.length; i++) {
                    marker[i].setMap(null);
                }
                poly.setMap(null);
                if (ponto_obstrucao !== false) {
                    ponto_obstrucao.setMap(null);
                }

                var bounds = new google.maps.LatLngBounds();
                var calculo_centro = [];
                var caminho = [];
                var cerca = $.parseJSON(msg);
                var num_pontos = cerca['poligono'].length;
//                map.panTo({lat: parseFloat(poligono['poligono'][0][0]), lng: parseFloat(poligono['poligono'][0][1])});
                for (i = 0; i < num_pontos; i++) {
                    calculo_centro[i] = new google.maps.LatLng(parseFloat(cerca['poligono'][i][0]), parseFloat(cerca['poligono'][i][1]));
                    marker[num_marks] = new google.maps.Marker({
                        position: {lat: parseFloat(cerca['poligono'][i][0]), lng: parseFloat(cerca['poligono'][i][1])},
                        title: "#" + i,
                        icon: iconBase + 'fence1.png',
                        map: map
                    });

                    caminho[i] = {lat: parseFloat(cerca['poligono'][i][0]), lng: parseFloat(cerca['poligono'][i][1])};
                    num_marks++;
                }

                ponto_obstrucao = new google.maps.Marker({
                    position: {lat: lat_fora_cerca, lng: lng_fora_cerca},
                    title: "Local de saida de cerca",
                    map: map
                });


                for (i = 0; i < num_pontos; i++) {
                    bounds.extend(calculo_centro[i]);
                }


                poly = new google.maps.Polyline({
                    path: caminho,
                    strokeColor: '#000000',
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                });

                poly.setMap(map);
                map.setCenter(bounds.getCenter());
                map.setZoom(parseInt(cerca['zoom']));


            });


            $('#modal_local_cerca').modal();

        });

        $('#modal_local_cerca').on('shown.bs.modal', function (e) {
            google.maps.event.trigger(map, "resize");
        })

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });

            poly = new google.maps.Polyline({
                strokeColor: '#000000',
                strokeOpacity: 1.0,
                strokeWeight: 3
            });
            poly.setMap(map);
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google_maps_key") ?>&callback=initMap" async defer></script>
</body>



</html>
