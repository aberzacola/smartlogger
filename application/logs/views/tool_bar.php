<div id="tool-bar" class="toolbat-background-color">
    <div  class="dropdown float-left toolbat-background-color" >
        <ul>
            <li>
                <a href="#"><img src="/assets/img/home.png" alt="Inicio" title="Inicio"></a>
                <ul class="dropdown" >
                    <li><a href="/" >Mapa de máquinas</a></li>
                    <li><a href="/maquinas/tabela" >Tabela de máquinas</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><img src="/assets/img/constructor2.png" alt="Configurações de Condutor" title="Configurações de Condutor"></a>
                <ul class="dropdown" >
                    <li><a href="/GotoMsg" >GoTo</a></li>
                    <li><a href="/condutores/cadastro/" >Cadastrar Condutor</a></li>
                    <li><a href="/condutores/lista/" >Lista de Condutores</a></li>
                    <li><a href="/condutores/historico/" >Histórico Condutores</a></li>
                </ul>

            </li>
            <li>
                <a href="#"><img src="/assets/img/fence1.png" alt="Configurações de Cerca" title="Cerca eletrônica"></a>
                <ul class="dropdown" >
                    <li><a href="/mapa/mapa_cria_cerca/" >Cadastrar Cerca</a></li>
                    <li><a href="/mapa/mapa_lista_cerca/" >Listar  Cercas</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><img src="/assets/img/alert11.png" alt="Alertas do Sistema" title="Alertas do Sistema"></a>
                <ul class="dropdown" >
                    <li><a href="/alertas/alertas_cerca_lista/" >Máquinas fora da cerca</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><img src="/assets/img/map4.png" alt="Rotas" title="Rotas"></a>
                <ul class="dropdown" >
                    <li><a href="/rotas/tracar_rotas/" >Traçar Rotas</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><img src="/assets/img/checklist.png" alt="Checklist" title="Checklist"></a>
                <ul class="dropdown" >
                    <li><a href="/checklist/criar_checklist/" >Criar Checklist</a></li>
                    <li><a href="/checklist/historico_checklist/" >Histórico Checklist</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><img src="/assets/img/weightlifting7.png" alt="Máquinas máquinas" title="Configurar máquinas"></a>
                <ul class="dropdown" >
                    <li><a href="/maquinas/cadastro/" >Cadastrar Máquina</a></li>
                    <li><a href="/maquinas/lista_cadastradas/" >Máquinas cadastradas</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><img src="/assets/img/fuel-station-pump.png" alt="Abastecimento" title="Abastecimento"></a>
                <ul class="dropdown" >
                    <li><a href="/abastecimento/abastecimento_data" >Abastecimento por data</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><img src="/assets/img/tool.png" alt="Manutenção" title="Manutenção"></a>
                <ul class="dropdown" >
                    <li><a href="/manutencao/preventiva_cadastro/" >Controle peças</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><img src="/assets/img/settings61.png" alt="Configurações Sistema" title="Configurações Sistema"></a>
                <ul class="dropdown" >
                    <li><a href="/configuracoes/posicao_padrao/" >Setar Ponto Padrão</a></li>
                    <li><a href="/configuracoes/cadastra_usuario/" >Cadastrar Usuários</a></li>
                    <li><a href="/configuracoes/lista_usuarios/" >Usuários Cadastrados</a></li>
                    <li><a href="/dados/consumo_entre_datas/" >Consumo de dados</a></li>
                </ul>
            </li>

        </ul>
    </div>
    <div class="float-right toolbat-background-color">
        <a href="/seguranca/logout/"><img src="/assets/img/power-button-off.png" alt="Sair" title="Sair"></a>
    </div>
</div>