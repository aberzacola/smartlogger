<html>
    <head>
        <title>Mapa - Cerca</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa_listar_cerca.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/simpleLittleTable.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>

            #map{
                width: 500px;
                height: 500px;
            }

        </style>


    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid" id="wrapper" >
            <div class="row espaco"></div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="maquina_deletada" class="alert alert-success hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        Máquina deletada com sucesso
                    </div>
                    <div id="maquina_deletada_erro" class="alert alert-danger hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        Erro ao deletar máquina
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-12" >
                    <table cellspacing='0'> <!-- cellspacing='0' is important, must stay -->
                        <tr>
                            <th>Id</th>
                            <th>Nome</th>
                            <th>Qr Code Ativação</th>
                            <th>Editar</th>
                            <th>Deletar</th>
                        </tr><!-- Table Header -->                
                        <?php
                        foreach ($maquinas_cadastradas as $key => $maquina) {
                            echo "<tr>"
                            . "<td><strong>{$maquina['id']}</strong></td>"
                            . "<td><strong>{$maquina['nome']}</strong></td>"
                            . "<td><strong><a target='_blank' href='/qr_code_gen/qr_img.php?d=Injetec Maquina,{$maquina['id']},{$maquina['nome']},{$dados_empresa['nome']},0,0,{$maquina['senha']}' >VER</a></strong></td>"
                            . "<td><a href='/maquinas/cadastro/{$maquina['id']}' ><img src='/assets/img/pencil-edit-button.png' alt='Editar Máquina' class='editar_maquina' title='Editar máquina' /></a>"
                            . "<td><a href='#' ><img src='/assets/img/delete96.png' alt='Deletar máquina' class='deletar_maquina' id_maquina='{$maquina['id']}' title='Deletar máquina' /></a>"
                            . "</tr>";
                        }
                        ?>

                    </table>

                </div>
            </div>
        </div>


    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script >

        $(".deletar_maquina").on("click", function () {
            var $estaMaquina = $(this);
            var confirma = confirm("Realmente deseja deletar esta maquina?");
            if (confirma === true) {
                $.ajax({
                    method: "POST",
                    url: "/ajax/ajax_maquina/deleta_maquina",
                    data: {id_maquina: $estaMaquina.attr("id_maquina")},
                    success: function () {
                        $("#maquina_deletada").removeClass("hidden");
                        $("#maquina_deletada_erro").addClass("hidden");
                        $estaMaquina.parents("tr").hide();
                    },
                    error: function () {
                        $("#maquina_deletada").addClass("hidden");
                        $("#maquina_deletada_erro").removeClass("hidden");
                    }
                });
            }
        })


    </script>
</body>



</html>
