<div id="tool-bar" class="dropdown" >
    <ul>
        <li>
            <a href="#"><img src="/assets/img/constructor2.png" alt="Configurações de Condutor" title="Configurações de Condutor"></a>
            <ul class="dropdown" >
                <li><a href="/condutores/cadastro/<?php echo $id_empresa; ?>" >Cadastrar Condutor</a></li>
                <li><a href="/condutores/lista/<?php echo $id_empresa; ?>" >Lista de Condutores</a></li>
            </ul>
        
        </li>
        <li>
            <a href="#"><img src="/assets/img/fence1.png" alt="Configurações de Cerca" title="Cerca eletrônica"></a>
            <ul class="dropdown" >
                <li><a href="/mapa/mapa_cria_cerca/<?php echo $id_empresa; ?>" >Cadastrar Cerca</a></li>
                <li><a href="/mapa/mapa_lista_cerca/<?php echo $id_empresa; ?>" >Listar  Cercas</a></li>
            </ul>
        </li>
        <li>
            <a href="#"><img src="/assets/img/settings61.png" alt="Configurações Sistema" title="Configurações Sistema"></a>
            <ul class="dropdown" >
                <li><a href="/configuracoes/posicao_padrao/<?php echo $id_empresa; ?>" >Setar Ponto Padrão</a></li>
            </ul>
        </li>
        <li>
            <a href="#"><img src="/assets/img/alert11.png" alt="Alertas do Sistema" title="Alertas do Sistema"></a>
            <ul class="dropdown" >
                <li><a href="/alertas/alertas_cerca_lista/<?php echo $id_empresa; ?>" >Máquinas fora da cerca</a></li>
            </ul>
        </li>
        <li>
            <a href="#"><img src="/assets/img/map4.png" alt="Rotas" title="Alertas do Sistema"></a>
            <ul class="dropdown" >
                <li><a href="/rotas/tracar_rotas/<?php echo $id_empresa; ?>" >Traçar Rotas</a></li>
            </ul>
        </li>
    </ul>
</div>
