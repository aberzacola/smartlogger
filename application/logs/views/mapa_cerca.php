<html>
    <head>
        <title>Mapa - Cerca</title>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFg39k6uZwhPglkZCu8AFqPxJAVNkB-YY&signed_in=true&callback=initMap"></script>
        <script async src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script async src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">


    </head>
    <body>
        <?php $this->load->view("tool_bar",array("id_router_gsm"=>$id_router_gsm));?>
        <div class="content-fluid" id="wrapper" >
            <div id="map"></div>
        </div>
        <script>
            var poly;
            var map;
            var marker = {};
            var num_marks = 0;
            var iconBase = "http://rastreio.injetec.com.br/assets/img/";

            function initMap() {
                
                
                
                map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 7,
                    center: {lat: 41.879, lng: -87.624}  // Center the map on Chicago, USA.
                });

                poly = new google.maps.Polyline({
                    strokeColor: '#000000',
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                });
                poly.setMap(map);

                // Add a listener for the click event
                map.addListener('click', addLatLng);
            }

// Handles click events on a map, and adds a new point to the Polyline.
            function addLatLng(event) {
                var path = poly.getPath();

                // Because path is an MVCArray, we can simply append a new coordinate
                // and it will automatically appear.
                path.push(event.latLng);

                // Add a new marker at the new plotted point on the polyline.
                marker[num_marks] = new google.maps.Marker({
                    position: event.latLng,
                    title: '#' + path.getLength(),
                    icon: iconBase + 'fence1.png',
                    map: map
                });
                var atual = num_marks;
                marker[num_marks].addListener('click',function(e){
                    var lat_primeiro_marker = marker[0].getPosition().lat();
                    var lng_primeiro_marker = marker[0].getPosition().lng();
                    
                    if(lat_primeiro_marker == e.latLng.lat() && lng_primeiro_marker == e.latLng.lng()){
                        path.push(e.latLng);
                        console.log("fechou a cerca");
                    }

                });
                num_marks++;
            }
            
            
                    
        
        </script>
    </body>



</html>
