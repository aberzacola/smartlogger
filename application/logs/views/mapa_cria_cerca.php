<html>
    <head>
        <title>Mapa - Cerca</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">


    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid" id="wrapper" >
            <div id="map"></div>
        </div>
        <div class="modal fade" id="modal_cadastro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Salvar Cerca eletrônica</h4>
                    </div>
                    <div class="modal-body">


                        <label for="exampleInputEmail1">Nome da cerca</label>
                        <input type="text" class="form-control" id="nomeCerca" name="nomeCerca" placeholder="Nome da cerca">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" id="salvar_cerca" class="btn btn-primary">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
        <script defer src="https://maps.googleapis.com/maps/api/js?key=<?=$this->config->item("google_maps_key")?>&callback=initMap"></script>
        <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script>
            var poly;
            var map;
            var teste;
            var values_latlon = [];
            var marker = [];
            var colisoes = [];
            var num_marks = 0;
            var iconBase = "http://rastreio.injetec.com.br/assets/img/";
            function initMap() {

<?php
if ($dados_empresa['lat_inicial'] == null) {
    echo "var myLatlng = {lat: -19.9178713, lng: -43.9603116};
          var zoom = 10;";
} else {
    echo "var myLatlng = {lat: {$dados_empresa['lat_inicial']}, lng: {$dados_empresa['lng_inicial']}};
          var zoom = {$dados_empresa['zoom_inicial']};";
}
?>

                map = new google.maps.Map(document.getElementById('map'), {
                    zoom: zoom,
                    zoomControl: true,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    center: myLatlng
                });


                poly = new google.maps.Polyline({
                    strokeColor: '#000000',
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                });
                poly.setMap(map);
                // Add a listener for the click event
                map.addListener('click', addLatLng);
            }

            // Handles click events on a map, and adds a new point to the Polyline.
            function addLatLng(event) {

                var path = poly.getPath();
                // Because path is an MVCArray, we can simply append a new coordinate
                // and it will automatically appear.
                // faz a linha
                path.push(event.latLng);

                // Add a new marker at the new plotted point on the polyline.
                marker[num_marks] = new google.maps.Marker({
                    position: event.latLng,
                    title: '#' + path.getLength() + " LAT: " + event.latLng.lat() + " LON" + event.latLng.lng(),
                    icon: iconBase + 'fence1.png',
                    map: map
                });


                marker[num_marks].addListener('click', function (e) {


                    var lat_primeiro_marker = marker[0].getPosition().lat();
                    var lng_primeiro_marker = marker[0].getPosition().lng();
                    var colisao_unica;
                    var avaliacao_colisao_local;
                    var valido = 1;
                    var houve_colisao = 0;
                    path.push(e.latLng);
                    if (lat_primeiro_marker == e.latLng.lat() && lng_primeiro_marker == e.latLng.lng()) {

                        // calculo para que linhas não se cruzem
                        for (i = 0; i < num_marks; i++) {
                            values_latlon.push([marker[i].getPosition().lat(), marker[i].getPosition().lng()]);
                        }

                        for (i = 0; i < num_marks; i++) {


                            var ponto_1_x = values_latlon[i][0];
                            var ponto_1_y = values_latlon[i][1];
                            if (i === num_marks - 1) {
                                var ponto_2_x = values_latlon[0][0];
                                var ponto_2_y = values_latlon[0][1];
                            } else {
                                var ponto_2_x = values_latlon[i + 1][0];
                                var ponto_2_y = values_latlon[i + 1][1];
                            }


                            for (j = 0; j < num_marks; j++) {

                                var ponto_3_x = values_latlon[j][0];
                                var ponto_3_y = values_latlon[j][1];
                                if (j === num_marks - 1) {
                                    var ponto_4_x = values_latlon[0][0];
                                    var ponto_4_y = values_latlon[0][1];
                                } else {
                                    var ponto_4_x = values_latlon[j + 1][0];
                                    var ponto_4_y = values_latlon[j + 1][1];
                                }
                                colisao_unica = get_line_intersection(ponto_1_x, ponto_1_y, ponto_2_x, ponto_2_y, ponto_3_x, ponto_3_y, ponto_4_x, ponto_4_y)
                                if (colisao_unica !== 0)
                                    colisoes.push(colisao_unica);
                            }

                        }

                        colisoes.pop();
                        colisoes.pop();

                        for (i = 0; i < colisoes.length; i++) {
                            avaliacao_colisao_local = 0;
                            for (j = 0; j < num_marks; j++) {
                                if (colisoes[i][0] === values_latlon[j][0] && colisoes[i][1] === values_latlon[j][1]) {
                                    avaliacao_colisao_local = 1;
                                }
                            }
                            if (avaliacao_colisao_local === 0) {
                                alert("Houve Colisão, as linhas não podem se sobrepor");
                                for (i = 0; i < num_marks; i++) {
                                    marker[i].setMap(null);
                                }
                                poly.setMap(null);
                                poly = new google.maps.Polyline({
                                    strokeColor: '#000000',
                                    strokeOpacity: 1.0,
                                    strokeWeight: 3
                                });
                                poly.setMap(map);
                                path = poly.getPath();
                                num_marks = 0;
                                marker = [];
                                colisoes = [];
                                values_latlon = [];
                                valido = 0;
                                break;
                            }

                        }
                        if (valido === 1)
                            $('#modal_cadastro').modal();
                    }

                });
                num_marks++;
            }

            function get_line_intersection(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y)
            {
                var s1_x, s1_y, s2_x, s2_y, i_x, i_y, s, t;

                s1_x = p1_x - p0_x;
                s1_y = p1_y - p0_y;
                s2_x = p3_x - p2_x;
                s2_y = p3_y - p2_y;

                s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
                t = (s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

                if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
                {
                    // Collision detected
                    if (i_x !== null)
                        i_x = p0_x + (t * s1_x);
                    if (i_y !== null)
                        i_y = p0_y + (t * s1_y);
                    return [i_x, i_y];
                }


                return 0; // No collision
            }

            $("#salvar_cerca").on("click", function () {



                var nome = $("#nomeCerca").val();
                var poligono_json;
                poligono_json = JSON.stringify(values_latlon);




                $.ajax({
                    method: "POST",
                    url: "/ajax/ajax_cerca/cadastra_cerca",
                    data: {id_empresa: <?php echo $id_empresa ?>, nome_cerca: nome, poligono: poligono_json, zoom: map.getZoom()}
                }).done(function (msg) {
                    if (msg === "1") {
                        alert("Cerca cadastrada com sucesso");
                    } else {
                        alert("Problema ao salvar a cerca");
                    }


                });



                $('#modal_cadastro').modal('hide');
                // limpando a tela após salvar 
                for (i = 0; i < num_marks; i++) {
                    marker[i].setMap(null);
                }
                poly.setMap(null);
                poly = new google.maps.Polyline({
                    strokeColor: '#000000',
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                });
                poly.setMap(map);
                path = poly.getPath();
                num_marks = 0;
                marker = [];
                colisoes = [];
                values_latlon = [];



            });


        </script>
    </body>



</html>
