<html>
    <head>
        <title>Alerta Lista</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa_rotas.css" />
        <link rel="stylesheet" type="text/css" href="/assets/datetimepicker/css/bootstrap-datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style>


        </style>

    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid" id="wrapper" >
            <div class="row">

                <div class="row espaco"></div>
                <div class="alert alert-info hidden" id="semData">
                    <strong>Selecione uma data</strong>
                </div>
                <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Máquina:
                    </div>
                    <div class='col-sm-10'>
                        <select class="form-control" id="idMaquina">
                            <?php
                            foreach ($maquinas as $key => $maquina) {
                                if ($key == 0)
                                    echo "<option selected value='{$maquina['id']}' >{$maquina['nome']}</option>";
                                else
                                    echo "<option value='{$maquina['id']}' >{$maquina['nome']}</option >";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Data Inicial:
                    </div>
                    <div class='col-sm-10 data'>
                        <div class="form-group">
                            <div class='input-group date' id='dataInicial'>
                                <input type='text' class="form-control" placeholder="Clique no calendário ao lado"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Data Final:
                    </div>
                    <div class='col-sm-10 data'>
                        <div class="form-group">
                            <div class='input-group date' id='dataFinal'>
                                <input type='text' class="form-control" placeholder="Clique no calendário ao lado"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class='col-sm-12 data'>
                        <button id="tracar-rota" type="button" class="btn btn-primary pull-right">Traçar rota</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modal_tracar_rota" tabindex="-1" role="dialog" aria-labelledby="modal_mapa_cerca_label">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modal_mapa_cerca_label">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div id="map">

                        </div>
                    </div>
                    <div class="hidden">
                        <div id="legend">
                            <h5>Legenda</h5>
                            <img width="15" src="/assets/img/inicio.png" /> Partida
                            <img width="15" src="/assets/img/direction.png" /> Direção
                            <img width="15" src="/assets/img/final.png" /> Fim
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/momentJs/moment.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/datetimepicker/js/bootstrap-datetimepicker.min.js" defer></script>




    <script >
        var map;
        var paths = [];
        var inicio;
        var fim;
        var seta;
        var poly;
        var num_marks = 0;
        var marker = [];
        var iconBase = "http://rastreio.injetec.com.br/assets/img/";
        var legend = document.getElementById('legend');
        $(document).ready(function () {

            $('#modal_tracar_rota').on('shown.bs.modal', function (e) {
                google.maps.event.trigger(map, "resize");
            })


            var dataInicialSetada = false;
            var dataFinalSetada = false;

            $('#dataInicial').datetimepicker({
                format: 'DD/MM/YYYY HH:mm'
            });
            $('#dataFinal').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD/MM/YYYY HH:mm'
            });
            $("#dataInicial").on("dp.change", function (e) {
//                $('#dataFinal').data("DateTimePicker").minDate(e.date);
                dataInicialSetada = true;

            });
            $("#dataFinal").on("dp.change", function (e) {
//                $('#dataInicial').data("DateTimePicker").maxDate(e.date);
                dataFinalSetada = true;
            });

            $("#tracar-rota").on("click", function () {
                if (dataInicialSetada == false) {
                    $("#semData").removeClass("hidden");
                    $("#semData strong").html("Selecione a data Inicial");
                    return;
                }

                if (dataFinalSetada == false) {
                    $("#semData").removeClass("hidden");
                    $("#semData strong").html("Selecione a data Final");
                    return;
                }

                var dataFinal = $("#dataFinal").data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:00');
                var dataInicial = $("#dataInicial").data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:00');

                var dataFinalComparacao = $("#dataFinal").data("DateTimePicker").date().format('X');
                var dataInicialComparacao = $("#dataInicial").data("DateTimePicker").date().format('X');

                if (dataFinalComparacao <= dataInicialComparacao) {
                    $("#semData").removeClass("hidden");
                    $("#semData strong").html("Data final não pode ser maior que a inicial");
                    return;
                }
                $("#semData").addClass("hidden");


                var maquina = $('#idMaquina').find(":selected").val();


                $("#modal_tracar_rota").modal('show');


                var img_tag = $(this).children("img");
                var id_trans_inicio = img_tag.attr("id_trans_inicio");
                var id_trans_final = img_tag.attr("id_trans_final");
                var id_cerca = img_tag.attr("id_cerca");
                var nome_cerca = img_tag.attr("nome_cerca");
                var bounds = new google.maps.LatLngBounds();



                $.ajax({
                    url: "/ajax/ajax_rotas/rota_entre_datas",
                    method: "POST",
                    dataType: "json",
                    data: {data_inicial: dataInicial, data_final: dataFinal, id_maquina: maquina},
                    success: function (locations) {
                        initMap();
                        paths = [];
                        for (var i = 0; i <= locations.length - 2; i++) {
                            if (i == 0) {
                                var coordinates = [
                                    {lat: parseFloat(locations[i].lat), lng: parseFloat(locations[i].lng)},
                                    {lat: parseFloat(locations[i + 1].lat), lng: parseFloat(locations[i + 1].lng)}
                                ];
                                if (locations.length == 2) {
                                    paths[i] = new google.maps.Polyline({
                                        path: coordinates,
                                        icons: [{
                                                icon: inicio,
                                                offset: '0%'
                                            }, {
                                                icon: seta,
                                                offset: '50%'
                                            }, {
                                                icon: fim,
                                                offset: '100%'
                                            }],
                                        geodesic: true,
                                        strokeColor: '#000',
                                        strokeOpacity: 1.0,
                                        strokeWeight: 2
                                    });
                                } else {
                                    paths[i] = new google.maps.Polyline({
                                        path: coordinates,
                                        icons: [{
                                                icon: inicio,
                                                offset: '0%'
                                            }, {
                                                icon: seta,
                                                offset: '50%'
                                            }],
                                        geodesic: true,
                                        strokeColor: '#000',
                                        strokeOpacity: 1.0,
                                        strokeWeight: 2
                                    });
                                }

                            } else if (i + 2 == locations.length) {
                                var coordinates = [
                                    {lat: parseFloat(locations[i].lat), lng: parseFloat(locations[i].lng)},
                                    {lat: parseFloat(locations[i + 1].lat), lng: parseFloat(locations[i + 1].lng)}
                                ];
                                paths[i] = new google.maps.Polyline({
                                    path: coordinates,
                                    icons: [{
                                            icon: seta,
                                            offset: '50%'
                                        }, {
                                            icon: fim,
                                            offset: '100%'
                                        }],
                                    geodesic: true,
                                    strokeColor: '#000',
                                    strokeOpacity: 1.0,
                                    strokeWeight: 2
                                });

                            } else {
                                var coordinates = [
                                    {lat: parseFloat(locations[i].lat), lng: parseFloat(locations[i].lng)},
                                    {lat: parseFloat(locations[i + 1].lat), lng: parseFloat(locations[i + 1].lng)}
                                ];
                                paths[i] = new google.maps.Polyline({
                                    path: coordinates,
                                    icons: [{
                                            icon: seta,
                                            offset: '50%'
                                        }],
                                    geodesic: true,
                                    strokeColor: '#000',
                                    strokeOpacity: 1.0,
                                    strokeWeight: 2
                                });

                            }
                        }
                        for (var i = 0; i <= locations.length - 2; i++) {
                            paths[i].setMap(map);
                        }
//                        for (var i = 0; i < locations.length; i++) {
//                            bounds.extend(new google.maps.LatLng(parseFloat(locations[i].lat), parseFloat(locations[i].lng)));
//                        }
                        google.maps.event.trigger(map, 'resize');
                        map.panTo(new google.maps.LatLng(parseFloat(locations[0].lat), parseFloat(locations[0].lng)));
                        map.setZoom(16);
                    }
                });
            });

        });

        function initMap() {

            map = null;

            inicio = {
                path: 'M -2,0 0,-2 2,0 0,2 z',
                strokeColor: '#292',
                fillColor: '#292',
                fillOpacity: 1
            };

            fim = {
                path: 'M -2,-2 2,2 M 2,-2 -2,2',
                strokeColor: '#F00',
                strokeWeight: 4
            };



            seta = {
                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                strokeColor: '#0000FF',
                fillColor: '#0000FF',
                fillOpacity: 1
            };



            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 8,
                center: {lat: 0, lng: -180},
                mapTypeId: google.maps.MapTypeId.TERRAIN
            });


            map.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= $this->config->item("google_maps_key") ?>" async defer></script>
</body>



</html>
