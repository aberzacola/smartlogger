<html>
    <head>
        <title>Condutores - Rastreio</title>
        <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script async src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script async src="/assets/blockUI/jquery.blockUI.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="/assets/css/maquinas.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="espaco">

        </div>
        <div class="container">

            <?php
            if ($id_maquina) {
                echo '<div id="cadastro_sucesso" class="alert alert-success d-none" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Máquina atualizada com sucesso
            </div>
            <div id="erro_desconhecido" class="alert alert-danger d-none" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Erro ao atualizada máquina, entre em contato com o suporte
            </div>';
            } else {
                echo '<div id="cadastro_sucesso" class="alert alert-success d-none" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Máquina cadastrada com sucesso
            </div>
            <div id="erro_desconhecido" class="alert alert-danger d-none" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                Erro ao cadastrar máquina, entre em contato com o suporte
            </div>';
            }
            ?>
            <form id="formulario_cadastro_maquina">
                <input type="text" class="d-none" id="id_maquina_atualizar" name="id_maquina" value="<?= $id_maquina ?>" />
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="nome_maquina">Nome da máquina</label>
                            <input type="text" class="form-control" id="nome_maquina" name="nome_maquina" placeholder="Nome Maquina" value="<?= isset($dados_maquina['nome']) ? $dados_maquina['nome'] : "" ?>">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="nome_maquina">Horas Horizontes</label>
                            <input type="text" class="form-control" id="horas_horizonte" name="horas_horizonte" placeholder="Horas Horizonte" value="<?= isset($dados_maquina['horas_horizonte']) ? $dados_maquina['horas_horizonte'] : "720" ?>">
                        </div>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="setPointTemperatura">Set point temperatura</label>
                            <input type="text" class="form-control" id="setPointTemperatura" name="setPointTemperatura" placeholder="Set point temperatura" value="<?= isset($dados_maquina['set_point_temperatura']) ? $dados_maquina['set_point_temperatura'] : "" ?>">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="setPointImpacto">Set point impacto</label>
                            <input type="text" class="form-control" id="setPointImpacto" name="setPointImpacto" placeholder="Set point impacto" value="<?= isset($dados_maquina['set_point_impacto']) ? $dados_maquina['set_point_impacto'] : "" ?>">
                        </div>
                    </div>

                </div>
                <div class="row" >
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="setPointTimeout">Set point timeout conexão</label>
                            <input type="text" class="form-control" id="setPointTimeout" name="setPointtimeout" placeholder="Minutos" value="<?= isset($dados_maquina['set_point_conexao_minutos']) ? $dados_maquina['set_point_conexao_minutos'] : "" ?>">
                        </div>
                    </div>

                </div>
                <div class="row" >
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="setPointVelocidade">Set point velocidade</label>
                            <input type="text" class="form-control" id="setPointVelocidade" name="setPointVelocidade" placeholder="Set point velocidade" value="<?= isset($dados_maquina['set_point_velocidade']) ? $dados_maquina['set_point_velocidade'] : "" ?>">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="ano">Tipo de combustível</label>
                            <select class="custom-select form-control" id="tipoCombustivel">
                                <option >Selecione</option>
                                <option <?= (isset($dados_maquina['tipo_combustivel']) && $dados_maquina['tipo_combustivel'] == "gas") ? "selected" : "" ?> value="gas">Gás</option>
                                <option <?= (isset($dados_maquina['tipo_combustivel']) && $dados_maquina['tipo_combustivel'] == "gasolina") ? "selected" : "" ?> value="gasolina">Gasolina</option>
                                <option <?= (isset($dados_maquina['tipo_combustivel']) && $dados_maquina['tipo_combustivel'] == "diesel") ? "selected" : "" ?> value="diesel">Diesel</option>
                            </select>
                        </div>
                    </div>

                </div>





                <?php
                if ($id_maquina) {
                    echo '<button type="submit" class="btn btn-default">Atualizar</button>';
                } else {
                    echo '<button type="submit" class="btn btn-default">Cadastrar</button>';
                }
                ?>
            </form>
        </div>
    </div>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script async src="/assets/blockUI/jquery.blockUI.js"></script>
    <script>


        $(document).ready(function () {

            $("#formulario_cadastro_maquina").on("submit", function (e) {
                e.preventDefault();

                var idMaquina = parseInt($("#id_maquina_atualizar").val());
                var nomeMaquina = $("#nome_maquina").val();
                var horasHorizonte = $("#horas_horizonte").val();
                var tipoCombustivel = $("#tipoCombustivel").val();
                var setPointTemperatura = $("#setPointTemperatura").val();
                var setPointImpacto = $("#setPointImpacto").val();
                var setPointVelocidade = $("#setPointVelocidade").val();
                var setPointTimeout = $("#setPointTimeout").val();

                if (idMaquina === 0) {
                    $("#formulario_cadastro_maquina").block({
                        message: '<h3>Castrando...</h3>'
                    });
                } else {
                    $("#formulario_cadastro_maquina").block({
                        message: '<h3>Atualizando...</h3>'
                    });
                }


                $.ajax({
                    url: "/ajax/ajax_maquina/cadastra_atualiza_maquina",
                    method: "POST",
                    dataType: "json",
                    data: {
                        idMaquina: idMaquina,
                        nome_maquina: nomeMaquina,
                        horas_horizonte: horasHorizonte,
                        setPointTemperatura: setPointTemperatura,
                        setPointImpacto: setPointImpacto,
                        setPointVelocidade: setPointVelocidade,
                        tipoCombustivel: tipoCombustivel,
                        setPointTimeout: setPointTimeout
                    },
                    success: function (e) {
                        if (e.response) {
                            $("#cadastro_sucesso").removeClass("d-none");
                            $("#erro_desconhecido").addClass("d-none");
                            $('#formulario_cadastro_maquina').unblock();
                            if (idMaquina === 0) {
                                $("#formulario_cadastro_maquina").trigger("reset");
                            }
                        } else {
                            $("#cadastro_sucesso").addClass("d-none");
                            $("#erro_desconhecido").removeClass("d-none");
                            $('#formulario_cadastro_maquina').unblock();
                            if (idMaquina === 0) {
                                $("#formulario_cadastro_maquina").trigger("reset");
                            }
                        }
                    },
                    error: function () {
                        $("#cadastro_sucesso").addClass("d-none");
                        $("#erro_desconhecido").removeClass("d-none");
                        $('#formulario_cadastro_maquina').unblock();
                        if (idMaquina === 0) {
                            $("#formulario_cadastro_maquina").trigger("reset");
                        }
                    }
                });
            });

        });


    </script> 
</body>




</html>
