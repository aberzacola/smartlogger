<html>
    <head>
        <title><?= $titulo_pagina ?></title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa_rotas.css" />
        <link rel="stylesheet" type="text/css" href="/assets/datetimepicker/css/bootstrap-datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" href="/assets/bootstrap-table/bootstrap-table.css">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style>


        </style>

    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid" id="wrapper" >
            <div class="row">
                <div class="row espaco"></div>
                <div class="col-lg-12">
                    <h3>Consumo de dados</h3>
                </div>
            </div>
            <div class="row">

                <div class="row espaco"></div>
                <div class="alert alert-info hidden" id="error">
                    <strong>Selecione uma data</strong>
                </div>
                <div class='row'>
                    <div class='col-lg-12'>
                        <label for="nome_maquina">Escolha a máquina </label>
                        <select class="form-control" id="id_maquina">
                            <?php
                            echo "<option value='0' >Escolha uma máquina</option>";
                            foreach ($maquinas as $maquina) {
                                $selected = "";
                                if ($id_maquina == $maquina['id']) {
                                    $selected = "selected";
                                }
                                echo "<option value='{$maquina['id']}' $selected>{$maquina['nome']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Data Inicial:
                    </div>
                    <div class='col-sm-10 data'>
                        <div class="form-group">
                            <div class='input-group date' id='dataInicial'>
                                <input type='text' class="form-control" placeholder="Clique no calendário ao lado"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class="col-sm-2 text-centert data">
                        Data Final:
                    </div>
                    <div class='col-sm-10 data'>
                        <div class="form-group">
                            <div class='input-group date' id='dataFinal'>
                                <input type='text' class="form-control" placeholder="Clique no calendário ao lado"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row espaco"></div>
                <div class="row">
                    <div class='col-sm-12 data'>
                        <button id="tracar-rota" type="button" class="btn btn-primary pull-right">Ver Resultados</button>
                    </div>
                </div>

            </div>
            <div class="espaco">

            </div>
            <div class="row hidden" id="dados_consumidos" >
                <div class="col-sm-12">
                    <h2>Dados consumidos no periodo:</h2>
                </div>
                <div class="col-sm-12">
                    <h2 id="qtd_tx"></h2>
                </div>
                <div class="col-sm-12">
                    <h2 id="qtd_rx"></h2>
                </div>
            </div>
        </div>

    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/momentJs/moment.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/datetimepicker/js/bootstrap-datetimepicker.min.js" defer></script>
    <script src="/assets/bootstrap-table/bootstrap-table.js"></script>

    <script >

        $(document).ready(function () {

            $('#table').bootstrapTable({});


            var dataInicialSetada = false;
            var dataFinalSetada = false;
            var idMaquina = false;

            $('#dataInicial').datetimepicker({
                format: 'DD/MM/YYYY HH:mm'
            });
            $('#dataFinal').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD/MM/YYYY HH:mm'
            });

            $("#dataInicial").on("dp.change", function (e) {
                $('#dataFinal').data("DateTimePicker").minDate(e.date);
                dataInicialSetada = true;

            });
            $("#dataFinal").on("dp.change", function (e) {
                $('#dataInicial').data("DateTimePicker").maxDate(e.date);
                dataFinalSetada = true;
            });

            $("#tracar-rota").on("click", function () {


                idMaquina = $("#id_maquina").val();
                if (idMaquina == 0) {
                    $("#error").removeClass("hidden");
                    $("#error strong").html("Selecione a uma maquina");
                    return;
                }

                if (dataInicialSetada == false) {
                    $("#error").removeClass("hidden");
                    $("#error strong").html("Selecione a data Inicial");
                    return;
                }

                if (dataFinalSetada == false) {
                    $("#error").removeClass("hidden");
                    $("#error strong").html("Selecione a data Final");
                    return;
                }

                $("#semData").addClass("hidden");

                var dataFinal = $("#dataFinal").data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:00');
                var dataInicial = $("#dataInicial").data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:00');


                $.ajax({
                    url: "/ajax/ajax_dados/consumo_por_periodo",
                    data: {data_inicio: dataInicial, data_final: dataFinal, id_maquina: idMaquina},
                    method: "POST",
                    dataType: "json",
                    success: function (resposta) {
                        $("#dados_consumidos").removeClass("hidden");
                        console.log(resposta.bytes_tx);
                        if (resposta.bytes_tx) {
                            $("#qtd_tx").text("TX: " + ((resposta.bytes_tx) / 1000000 ) + " MB") ;
                        } else {
                            $("#qtd_tx").text("Dados insuficientes");
                        }
                        
                        if (resposta.bytes_rx) {
                            $("#qtd_rx").text("RX : " + ((resposta.bytes_rx) / 1000000 ) + " MB") ;
                        } else {
                            $("#qtd_rx").text("Dados insuficientes");
                        }
                    }
                });


            });
        });
    </script>
</body>



</html>
