<html>
    <head>
        <title>Manutenção - Preventiva Cadastro</title>
        <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script async src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script async src="/assets/blockUI/jquery.blockUI.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="/assets/css/maquinas.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />

        <link rel="stylesheet" href="/assets/bootstrap-table/bootstrap-table.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div id="wrapper">
            <div class='container-fluid'>
                <form id="formulario_cadastro_maquina">
                    <div class='row'>
                        <div class='col-lg-12'>
                            <label for="nome_maquina">Escolha a máquina </label>
                            <select class="form-control" id="id_maquina">
                                <?php
                                echo "<option value='0' >Escolha uma máquina</option>";
                                foreach ($maquinas as $maquina) {
                                    $selected = "";
                                    if ($id_maquina == $maquina['id']) {
                                        $selected = "selected";
                                    }
                                    echo "<option value='{$maquina['id']}' $selected>{$maquina['nome']}</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class='espaco' ></div>
                    <div class='row' >
                        <div class='col-lg-12'>
                            <button type="submit" class="btn btn-default">ver</button>
                        </div>
                    </div>
                </form>
                <?php
                if ($id_maquina != 0) {
                    ?>
                    <hr/>
                    <div class='row'>
                        <div class="col-xs-2">
                            <h2>Peças cadastradas</h2>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cadastrar_peca">
                                Cadastrar Peça
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div id="cadastro_sucesso" class="col-lg-12 alert alert-success d-none" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                            Operador cadastrado com sucesso
                        </div>
                        <div id="operador_existente" class="col-lg-12 alert alert-danger d-none" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                            Operador já cadastrado
                        </div>
                        <div id="erro_desconhecido" class="col-lg-12 alert alert-danger d-none" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                            Erro ao cadastrar operador, entre em contato com o suporte
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-lg-12 margem">
                            <table class="table table-striped "  
                                   id="tabela_manutencao"
                                   data-toggle="table" 
                                   data-sort-order="asc" 
                                   data-height="250"
                                   data-toolbar="#toolbar"
                                   data-sort-name="tempo_restante" 
                                   >
                                <thead>
                                    <tr>
                                        <th data-field="nome" data-sortable="true"  >Nome</th>
                                        <th data-field="tempo_restante" data-sortable="true"  >Tmp. Restante</th>
                                        <th data-field="tempo_maximo" data-sortable="true">Tmp. Limite</th>
                                        <th data-field="obs" data-sortable="true">Obs</th>
                                        <th data-field="zerar" data-sortable="true">Zerar</th>
                                        <th data-field="deletar" data-sortable="true">Deletar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($pecas as $peca){
                                        echo '<tr id="tr-id-1" class="tr-class-1">';
                                        echo '<td>'.$peca['nome'].'</td>';
                                        echo '<td>'.$peca['tempo_restante'].'</td>';
                                        echo '<td>'.$peca['tempo_limite'].'</td>';
                                        echo '<td>'.$peca['obs'].'</td>';
                                        echo '<td ><center><img src="/assets/img/update-arrow.png" title="zerar" alt="zerar"/></center></td>';
                                        echo '<td><center><img src="/assets/img/delete96.png" title="Deletar" alt="Deletar"/></center></td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                    
                                       
                                        
                                        
                                        
                                        
                                
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>

        </div>

        <!-- Modal -->
        <div class="modal fade" id="cadastrar_peca" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Cadastrar nova manutenção</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <form id="form_cadastra_peca">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="nome">Nome manutencao</label>
                                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite um nome">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="tempoDeUso">Tempo já utilizado</label>
                                            <input type="number" class="form-control" id="tempoDeUso" name="nome" placeholder="tempo que já está em uso">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="tempoDeUso">Tempo Limite</label>
                                            <input type="number" class="form-control" id="tempoLimiteDeUso" name="nome" placeholder="Tempo Limite de Uso">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="observacao">Observação</label>
                                            <textarea class="form-control" id="observacao" rows="3" placeholder="Deixe uma Observação"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sair</button>
                        <button id="salvar_peca" type="button" class="btn btn-primary">Salvar</button>
                    </div>
                </div>
            </div>
        </div>


        <script src="/assets/jquery/js/jquery-3.2.1.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
        <script async src="/assets/blockUI/jquery.blockUI.js"></script>
        <script src="/assets/bootstrap-table/bootstrap-table.js"></script>
        <script>


            $(document).ready(function () {

                $('#cadastrar_peca').on('hidden.bs.modal', function (e) {
                    console.log("a");
                });
                $("#formulario_cadastro_maquina").on("submit", function (e) {
                    e.preventDefault();
                    window.location = "/manutencao/preventiva_cadatro_detalhe/" + $("#id_maquina").val();
                });

                $("#salvar_peca").on("click", function () {

                    $('#cadastrar_peca').block({
                        message: '<h3>Cadastrando...</h3>'
                    });

                    var id_maquina = $("#id_maquina").val();
                    var nome = $("#nome").val();
                    var tempoDeUso = $("#tempoDeUso").val();
                    var tempoLimiteDeUso = $("#tempoLimiteDeUso").val();
                    var observacao = $("#observacao").val();
                    data = {
                        id_maquina: id_maquina,
                        nome: nome,
                        tempo_utilizado: tempoDeUso,
                        tempo_limite: tempoLimiteDeUso,
                        observacao: observacao,
                    };

                    $.ajax({
                        url: "/ajax/ajax_manutencao/cadastra_manutencao",
                        method: "POST",
                        data: data,
                        success: function () {
                            $('#cadastrar_peca').unblock();
                            $("#form_cadastra_peca").trigger("reset");
                        }
                    });

                });
            });


        </script> 
    </body>




</html>
