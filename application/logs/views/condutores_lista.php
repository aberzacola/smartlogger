<html>
    <head>
        <title>Mapa - Cerca</title>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/mapa_listar_cerca.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/simpleLittleTable.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>

            #map{
                width: 500px;
                height: 500px;
            }

        </style>
    </head>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid" id="wrapper" >
            <div class="row espaco"></div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="maquina_deletada" class="alert alert-success hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        Condutor deletada com sucesso
                    </div>
                    <div id="maquina_deletada_erro" class="alert alert-danger hidden" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        Erro ao deletar condutor
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-lg-12" >
                    <table cellspacing='0'> <!-- cellspacing='0' is important, must stay -->
                        <tr>
                            <th>Nome Condutor</th>
                            <th>Tipo Condutor</th>
                            <th>Data Exame Medico</th>
                            <th>Data Treinamento</th>
                            <th>Qr Code</th>
                            <th>Deletar</th>
                        </tr><!-- Table Header -->                
                        <?php
                        foreach ($condutores as $key => $condutor) {
                            if ($condutor["condutor"] == 1) {
                                $tipoQrCode = "Operador";
                            } else if ($condutor["abastecedor"] == 1) {
                                $tipoQrCode = "Abastecimento";
                            } else {
                                $tipoQrCode = "Mecanico";
                            }
                            
                            $datas = "";
                            $data_exame = "";
                            $data_treinamento = "";
                            if($condutor['data_exame_medico'] != NULL){
                                $date_explode = explode("-",$condutor['data_exame_medico']);
                                $datas .= $date_explode[2]."/".$date_explode[1]."/".$date_explode[0];
                                $data_exame = $date_explode[2]."/".$date_explode[1]."/".$date_explode[0];
                            }else{
                                $datas .= "01/01/2099";
                            }
                            if($condutor['data_exame_treinamento'] != NULL){
                                $date_explode = explode("-",$condutor['data_exame_treinamento']);
                                $datas .= ",".$date_explode[2]."/".$date_explode[1]."/".$date_explode[0];
                                $data_treinamento = $date_explode[2]."/".$date_explode[1]."/".$date_explode[0];
                            }else {
                                $datas .= ",01/01/2099";
                            }
                            
                            echo "<tr>"
                            . "<td><strong>{$condutor['nome']}</strong></td>"
                            . "<td><strong>$tipoQrCode</strong></td>"
                            . "<td><strong>$data_exame</strong></td>"
                            . "<td><strong>$data_treinamento</strong></td>"
                            . "<td><strong><a target='_blank' href='/qr_code_gen/qr_img.php?d=Injetec $tipoQrCode,{$condutor['nome']},{$condutor['id']},$datas' >VER</a></strong></td>"
                            . "<td><a href='#' ><img src='/assets/img/delete96.png' alt='Deletar condutor' class='deletar_condutor' id_condutor='{$condutor['id']}' title='Deletar condutor' /></a>"
                            . "</tr>";
                        }
                        ?>

                    </table>
                </div>
            </div>
        </div>

    </body>
    <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script >

        $(".deletar_condutor").on("click", function () {
            var $esteCondutor = $(this);
            var confirma = confirm("Realmente deseja deletar este condutor?");
            if (confirma === true) {
                $.ajax({
                    method: "POST",
                    url: "/ajax/ajax_condutores/deleta_condutor",
                    data: {id_condutor: $esteCondutor.attr("id_condutor")},
                    success: function () {
                        $("#maquina_deletada").removeClass("hidden");
                        $("#maquina_deletada_erro").addClass("hidden");
                        $esteCondutor.parents("tr").hide();
                    },
                    error: function () {
                        $("#maquina_deletada").addClass("hidden");
                        $("#maquina_deletada_erro").removeClass("hidden");
                    }
                });
            }
        })


    </script>
</body>



</html>
