<html>
    <head>
        <title>Tabela de máquinas - Rastreio</title>
        <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script async src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script async src="/assets/blockUI/jquery.blockUI.js"></script>
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tabela_maquinas.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/tool_bar.css" />
        <link rel="stylesheet" href="/assets/bootstrap-table/bootstrap-table.css">


        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        .horas_trabalhadas {
            display: block;
        }
        .screenFiller {
            position: absolute;
            top: 40px; right: 0; bottom: 55px; left: 0;
            z-index: 0;
        }
    </style>
    <body>
        <?php $this->load->view("tool_bar"); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 margem screenFiller">
                    <div id="toolbar">
                        <button id="button_refresh" title="Clique para atualizar a tabela" class="btn btn-default">
                            <img src='/assets/img/refresh-button.png' alt='Atualziar tabela' title="Atualizar tabela" /></button>
                        <button id="button_graficos" title="Ver gráficos" class="btn btn-default">Gerar Graficos</button>
                    </div>
                    <table class="table table-striped"  
                           id="tabela_maquinas"
                           data-toggle="table" 
                           data-sort-order="asc" 
                           data-toolbar="#toolbar"
                           data-sort-name="atualizado" 
                           data-url="/ajax/ajax_maquina/tabela_todas_maquinas"
                           >
                        <thead>
                            <tr>
                                <th data-field="select" data-checkbox="true"></th>
                                <th data-field="id" class="hidden" >id</th>
                                <th data-field="nome" data-sortable="true"  >Máquina</th>
                                <th data-field="operador" data-sortable="true"  >Operador</th>
                                <!--<th data-field="localidade" data-sortable="true">Local</th>-->
                                <th data-field="horas_trabalhadas" data-sortable="true">Horas <br />Trabalhadas(h)</th>
                                <!--<th data-field="hodometro" data-sortable="true">Hodometro(Km)</th>-->
                                <!--<th data-field="iu" data-sortable="true">I.U (%)</th>-->
                                <!--<th data-field="hora_efetiva" data-sortable="true">h. EF(%)</th>-->
                                <!--<th data-field="hora_efetiva_horas" data-sortable="true">h. EF(h)</th>-->
                                <th data-field="atualizado" data-sortable="true" >Atualizado</th>
                                <!--<th data-field="temperatura" data-sortable="true" >Temperatura(°C)</th>-->
                                <th data-field="temperatura_pico" data-sortable="true" >Temp. Pico(°C)</th>
                                <th data-field="acelerometro" data-sortable="true" >Impacto(G)</th>
                                <th data-field="cinto" data-sortable="true" >Estado do cinto</th>
                                <!--<th data-field="status_rele" data-sortable="true" >Status Rele</th>-->
                                <th data-field="nivel_celular" data-sortable="true" >Bat. Equip (%)</th>
                                <th data-field="vel_max" data-sortable="true">Vel. Max( Km/h )</th>
                                <th data-field="cerca" data-sortable="true">Cerca</th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>

        </div>

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_graficos">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gráficos últimos 30 dias</h4>
                    </div>
                    <div class="modal-body" >
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class='horimetro'></div>
                                    <div class='acelerometro'></div>
                                    <div class='hodometro_parcial'></div>
                                    <div class='hora_efetiva'></div>
                                    <div class='velocidade'></div>
                                    <div class='temperatura'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <script src="/assets/jquery/js/jquery-2.1.4.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="/assets/momentJs/moment.min.js"></script>
        <script async src="/assets/blockUI/jquery.blockUI.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="/assets/bootstrap-table/bootstrap-table.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script>

            google.charts.load('current', {packages: ['corechart', 'line']});
            google.charts.setOnLoadCallback(initiateChartVariable);

            var $table = $('#tabela_maquinas'),
                    $button = $("#button_refresh"),
                    $button_graficos = $("#button_graficos"),
                    $grafico_area = $(".graficos");
            var segundos_restantes = 60;
            var counter = 0;
            var chartVelocidade,
                    chartTemperatura,
                    chartHodometroParcial,
                    chartHoraEfetiva,
                    chartAcelerometro,
                    chartHorimetro;

            var dados_graficos = new Array();

            $button.click(function () {
                $table.bootstrapTable("refresh");
            });

            $button_graficos.on("click", function () {
                $('#modal_graficos').modal('toggle');
            });


            $('#modal_graficos').on('shown.bs.modal', function (e) {
                var dataVelocidade = new google.visualization.DataTable();
                var dataTemperatura = new google.visualization.DataTable();
                var dataHodometroParcial = new google.visualization.DataTable();
                var dataHoraEfetiva = new google.visualization.DataTable();
                var dataAcelerometro = new google.visualization.DataTable();
                var dataHorimetro = new google.visualization.DataTable();
                var maquinas = new Array();

                dataVelocidade.addColumn('date', 'date');
                dataTemperatura.addColumn('date', 'date');
                dataHodometroParcial.addColumn('date', 'date');
                dataHoraEfetiva.addColumn('date', 'date');
                dataAcelerometro.addColumn('date', 'date');
                dataHorimetro.addColumn('date', 'date');


                for (var i = 0; i < $table.bootstrapTable('getSelections').length; i++) {
                    maquinas[i] = $table.bootstrapTable('getSelections')[i].id;
                    dataVelocidade.addColumn('number', $table.bootstrapTable('getSelections')[i].nome);
                    dataTemperatura.addColumn('number', $table.bootstrapTable('getSelections')[i].nome);
                    dataHodometroParcial.addColumn('number', $table.bootstrapTable('getSelections')[i].nome);
                    dataHoraEfetiva.addColumn('number', $table.bootstrapTable('getSelections')[i].nome);
                    dataAcelerometro.addColumn('number', $table.bootstrapTable('getSelections')[i].nome);
                    dataHorimetro.addColumn('number', $table.bootstrapTable('getSelections')[i].nome);
                }

                dataVelocidade.addColumn({type: 'string', role: 'tooltip'});
                dataTemperatura.addColumn({type: 'string', role: 'tooltip'});
                dataHodometroParcial.addColumn({type: 'string', role: 'tooltip'});
                dataHoraEfetiva.addColumn({type: 'string', role: 'tooltip'});
                dataAcelerometro.addColumn({type: 'string', role: 'tooltip'});
                dataHorimetro.addColumn({type: 'string', role: 'tooltip'});


                $.when(
                        $.ajax({
                            url: "/ajax/ajax_maquina/last_30_dias_horimetro",
                            dataType: "json",
                            method: "POST",
                            data: {ids_maquinas: maquinas}
                        }),
                        $.ajax({
                            url: "/ajax/ajax_maquina/last_30_dias_acelerometro",
                            dataType: "json",
                            method: "POST",
                            data: {ids_maquinas: maquinas}
                        }),
                        $.ajax({
                            url: "/ajax/ajax_maquina/last_30_dias_hora_efetiva",
                            dataType: "json",
                            method: "POST",
                            data: {ids_maquinas: maquinas}
                        }),
                        $.ajax({
                            url: "/ajax/ajax_maquina/last_30_dias_hodometro_parcial",
                            dataType: "json",
                            method: "POST",
                            data: {ids_maquinas: maquinas}
                        }),
                        $.ajax({
                            url: "/ajax/ajax_maquina/last_30_dias_velocidade",
                            dataType: "json",
                            method: "POST",
                            data: {ids_maquinas: maquinas}
                        }),
                        $.ajax({
                            url: "/ajax/ajax_maquina/last_30_dias_temperatura",
                            dataType: "json",
                            method: "POST",
                            data: {ids_maquinas: maquinas}
                        })
                        ).then(function (horimetro, acelerometro, horas_efetiva, hodometro_parcial, velocidade, temperatura) {
                    horas_efetiva = horas_efetiva[0];
                    velocidade = velocidade[0];
                    temperatura = temperatura[0];
                    hodometro_parcial = hodometro_parcial[0];
                    acelerometro = acelerometro[0];
                    horimetro = horimetro[0];

                    for (var i = 0; i < horimetro.length; i++) {
                        var custom_tooltip = "Data: " + moment(horimetro[i][0]).format("DD/MM/YYYY HH:mm")
                                + "\nHorimetro: " + horimetro[i][1];

                        var provisorio = new Array(new Date(horimetro[i][0]));
                        horimetro[i].shift();
                        provisorio = provisorio.concat(horimetro[i]);
                        provisorio = provisorio.concat(custom_tooltip);
                        dataHorimetro.addRow(provisorio);
                    }
                    for (var i = 0; i < acelerometro.length; i++) {
                        var custom_tooltip = "Data: " + moment(acelerometro[i][0]).format("DD/MM/YYYY HH:mm")
                                + "\nAcelerometro: " + acelerometro[i][1];

                        var provisorio = new Array(new Date(acelerometro[i][0]));
                        acelerometro[i].shift();
                        provisorio = provisorio.concat(acelerometro[i]);
                        provisorio = provisorio.concat(custom_tooltip);
                        dataAcelerometro.addRow(provisorio);
                    }
                    for (var i = 0; i < horas_efetiva.length; i++) {
                        var custom_tooltip = "Data: " + moment(horas_efetiva[i][0]).format("DD/MM/YYYY HH:mm")
                                + "\n Horas Efetivas: " + horas_efetiva[i][1];

                        var provisorio = new Array(new Date(horas_efetiva[i][0]));
                        horas_efetiva[i].shift();
                        provisorio = provisorio.concat(horas_efetiva[i]);
                        provisorio = provisorio.concat(custom_tooltip);
                        dataHoraEfetiva.addRow(provisorio);
                    }
                    for (var i = 0; i < hodometro_parcial.length; i++) {
                        var custom_tooltip = "Data: " + moment(hodometro_parcial[i][0]).format("DD/MM/YYYY HH:mm")
                                + "\n Hodometro Parcial: " + hodometro_parcial[i][1];

                        var provisorio = new Array(new Date(hodometro_parcial[i][0]));
                        hodometro_parcial[i].shift();
                        provisorio = provisorio.concat(hodometro_parcial[i]);
                        provisorio = provisorio.concat(custom_tooltip);
                        dataHodometroParcial.addRow(provisorio);
                    }
                    for (var i = 0; i < velocidade.length; i++) {
                        var custom_tooltip = "Data: " + moment(velocidade[i][0]).format("DD/MM/YYYY HH:mm")
                                + "\n Velocidade Inst.: " + velocidade[i][1];

                        var provisorio = new Array(new Date(velocidade[i][0]));
                        velocidade[i].shift();
                        provisorio = provisorio.concat(velocidade[i]);
                        provisorio = provisorio.concat(custom_tooltip);
                        dataVelocidade.addRow(provisorio);
                    }
                    for (var i = 0; i < temperatura.length; i++) {
                        var custom_tooltip = "Data: " + moment(temperatura[i][0]).format("DD/MM/YYYY HH:mm")
                                + "\n Temperatura: " + temperatura[i][1];
                        
                        var provisorio = new Array(new Date(temperatura[i][0]));
                        temperatura[i].shift();
                        provisorio = provisorio.concat(temperatura[i]);
                        provisorio = provisorio.concat(custom_tooltip);
                        dataTemperatura.addRow(provisorio);
                    }
                    gerar_grafico_velocidade(dataVelocidade);
                    gerar_grafico_temperatura(dataTemperatura);
                    gerar_grafico_hodometro_parcial(dataHodometroParcial);
                    gerar_grafico_hora_efetiva(dataHoraEfetiva);
                    gerar_grafico_acelerometro(dataAcelerometro);
                    gerar_grafico_horimetro(dataHorimetro);
                });


            });

            function gerar_grafico_horimetro(data) {

                var options = {
                    height: 350,
                    hAxis: {
                        title: 'Data',
                    },
                    vAxis: {
                        title: 'Horimetro'
                    },
                    explorer: {}
                };

                chartHorimetro.draw(data, options);
            }
            function gerar_grafico_acelerometro(data) {

                var options = {
                    height: 350,
                    hAxis: {
                        title: 'Data'
                    },
                    vAxis: {
                        title: 'Acelerometro'
                    },
                    explorer: {}
                };

                chartAcelerometro.draw(data, options);
            }
            function gerar_grafico_hora_efetiva(data) {

                var options = {
                    height: 350,
                    hAxis: {
                        title: 'Data'
                    },
                    vAxis: {
                        title: 'Hora Efetiva'
                    },
                    explorer: {}
                };

                chartHoraEfetiva.draw(data, options);
            }
            function gerar_grafico_velocidade(data) {

                var options = {
                    height: 350,
                    hAxis: {
                        title: 'Data'
                    },
                    vAxis: {
                        title: 'Velocidade Instantânea'
                    },
                    explorer: {}
                };

                chartVelocidade.draw(data, options);
            }
            function gerar_grafico_temperatura(data) {

                var options = {
                    height: 350,
                    hAxis: {
                        title: 'Data'
                    },
                    vAxis: {
                        title: 'Temperatura Pico'
                    },
                    explorer: {}
                };

                chartTemperatura.draw(data, options);
            }
            function gerar_grafico_hodometro_parcial(data) {

                var options = {
                    height: 350,
                    hAxis: {
                        title: 'Data'
                    },
                    vAxis: {
                        title: 'Hodometro Parcial'
                    },
                    explorer: {}
                };

                chartHodometroParcial.draw(data, options);
            }


            function initiateChartVariable() {
                chartVelocidade = new google.visualization.LineChart(document.getElementsByClassName('velocidade')[0]);
                chartTemperatura = new google.visualization.LineChart(document.getElementsByClassName('temperatura')[0]);
                chartHodometroParcial = new google.visualization.LineChart(document.getElementsByClassName('hodometro_parcial')[0]);
                chartHoraEfetiva = new google.visualization.LineChart(document.getElementsByClassName('hora_efetiva')[0]);
                chartAcelerometro = new google.visualization.LineChart(document.getElementsByClassName('acelerometro')[0]);
                chartHorimetro = new google.visualization.LineChart(document.getElementsByClassName('horimetro')[0]);
            }


        </script> 

    </body>




</html>
