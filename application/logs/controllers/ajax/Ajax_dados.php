<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_dados extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function consumo_por_periodo() {

        $this->load->model("Dados_model", "", TRUE);

        $id_maquina = $this->input->post("id_maquina", TRUE);
        $data_inicial = $this->input->post("data_inicio", TRUE);
        $data_final = $this->input->post("data_final", TRUE);


        $dados = $this->Dados_model->get_dados_entre_trans($id_maquina, $data_inicial, $data_final);

        if (count($dados) >= 2) {
            $total['bytes_tx'] = $dados[count($dados) - 1]['bytes_tx'] - $dados[0]['bytes_tx'];
            $total['bytes_rx'] = $dados[count($dados) - 1]['bytes_rx'] - $dados[0]['bytes_rx'];
        }else{
            $total['bytes_tx'] = false;
            $total['bytes_rx'] = false;
        }

        $data['resposta'] = json_encode($total);
        $this->load->view("ajax/ajax_resposta", $data);
    }

}
