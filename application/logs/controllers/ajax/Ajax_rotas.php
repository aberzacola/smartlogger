<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_rotas extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function rota_entre_trans() {

        $this->load->model("Rotas_model", "", TRUE);


        $id_trans_inicio = $this->input->post("id_trans_inicio", TRUE);
        $id_trans_final = $this->input->post("id_trans_final", TRUE);

        $resposta = $this->Rotas_model->get_intervalo_id_trans($id_trans_inicio, $id_trans_final);

        $data['resposta'] = json_encode($resposta);

        $this->load->view("ajax/ajax_resposta", $data);
    }
    
    public function rota_entre_datas(){
        $this->load->model("Rotas_model", "", TRUE);


        $data_inicio = $this->input->post("data_inicial", TRUE);
        $data_final = $this->input->post("data_final", TRUE);
        $id_maquina = $this->input->post("id_maquina", TRUE);
        
        $resposta = $this->Rotas_model->get_intervalo_datas_trans($data_inicio,$data_final,$id_maquina);
        
        $data['resposta'] = json_encode($resposta);

        $this->load->view("ajax/ajax_resposta", $data);
    }

}
