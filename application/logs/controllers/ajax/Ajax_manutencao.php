<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_manutencao extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function cadastra_manutencao() {

        $this->load->model("manutencao_model", "", TRUE);
        $this->load->model("maquina_model", "", TRUE);

        $id_empresa = $_SESSION['id_empresa'];

        $id_maquina = $this->input->post("id_maquina", TRUE);
        $nome = $this->input->post("nome", TRUE);
        $tempo_utilizado = $this->input->post("tempo_utilizado", TRUE);
        $tempo_limite = $this->input->post("tempo_limite", TRUE);
        $observacao = $this->input->post("observacao", TRUE);

        $last_transmission = $this->maquina_model->get_last_transmission($id_maquina);

        $this->manutencao_model->set_peca_nova($id_empresa, $id_maquina, $nome, $last_transmission[0]['id'], $tempo_utilizado,$tempo_limite,$observacao);
    }

}
