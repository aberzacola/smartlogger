<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_condutores extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function checa_existencia() {

        $this->load->model("Condutores_model", "", TRUE);

        // pegando ID do operador
        $id_identificador = $this->input->post("id_identificador", TRUE);

        if ($id_identificador == NULL) {
            $this->load->view("ajax/ajax_resposta", array("resposta" => "0"));
        } else {
            //Checando a existencia do operador
            $resposta = $this->Condutores_model->checa_id($id_identificador);

            //convertendo TRUE e FALSE para 1 e 0 
            $resposta = ($resposta == TRUE) ? 1 : 0;

            //fazendo json
            $resposta = json_encode($resposta);

            $this->load->view("ajax/ajax_resposta", array("resposta" => $resposta));
        }
    }

    public function cadastra_condutor() {

        $this->load->model("Condutores_model", "", TRUE);

        // recebendo o POST
        $nome = $this->input->post("nome", TRUE);
        $tipoCondutor = $this->input->post("tipoCondutor", TRUE);
        $dataTreinamento = $this->input->post("dataTreinamento", TRUE) == "" ? NULL : $this->input->post("dataTreinamento", TRUE);
        $dataExameMedico = $this->input->post("dataExameMedico", TRUE) == "" ? NULL : $this->input->post("dataExameMedico", TRUE);

        $id_empresa = $this->input->post("id_empresa", TRUE);

        // Fazendo o cadastro
        if ($nome != NULL && $id_empresa != NULL) {
            $this->Condutores_model->cadastra_operador($nome, $id_empresa, $tipoCondutor, $dataTreinamento, $dataExameMedico);
            $this->load->view("ajax/ajax_resposta", array("resposta" => "1"));
        } else {
            // deu errado retorna 0
            $this->load->view("ajax/ajax_resposta", array("resposta" => "0"));
        }
    }

    //Deleta condutor
    public function deleta_condutor() {

        $this->load->model("Condutores_model", "", TRUE);

        $id_condutor = $this->input->post("id_condutor", TRUE);

        $this->Condutores_model->deleta_condutor($id_condutor);

        $resposta = 1;

        $this->load->view("ajax/ajax_resposta", array("resposta" => $resposta));
    }

    //Pega as infos do histórico dos condutores
    public function historico_condutor() {

        $this->load->model("Condutores_model", "", TRUE);

//        $id_condutor = "105";
//        $nome_condutor = "Angelo";
//        $dataFinal = "2017-11-01 18:46:00";
//        $dataInicial = "2017-10-01 18:46:00";
        $id_condutor = $this->input->post("id_condutor", TRUE);
        $dataFinal = $this->input->post("dataFinal", TRUE);
        $dataInicial = $this->input->post("dataInicial", TRUE);
        $nome_condutor = $this->input->post("nome_condutor", TRUE);

        $num_jornadas = 0;
        $resultado = $this->Condutores_model->get_all_trans_between($id_condutor, $dataInicial, $dataFinal);
        foreach ($resultado as $transmissao) {
            if (!isset($comeco_jornada)) {

                $comeco_jornada = $transmissao['status_rele'];

                if ($comeco_jornada == 0) {
                    $jornadas[$num_jornadas]['inicio'] = $transmissao;
                } else {
                    $jornadas[$num_jornadas]['termino'] = $transmissao;
                    $num_jornadas++;
                }
            } else {

                if (!isset($last_jornada)) {
                    $last_jornada = $num_jornadas;
                }

                if ($comeco_jornada != $transmissao['status_rele']) {
                    $comeco_jornada = $transmissao['status_rele'];

                    if ($comeco_jornada == 0) {
                        $jornadas[$num_jornadas]['inicio'] = $transmissao;
                    } else {
                        $jornadas[$num_jornadas]['termino'] = $transmissao;
                        $num_jornadas++;
                    }
                }
            }
        }

        $jornadas = array_reverse($jornadas);

        $somatorio_horas = 0;
        foreach ($jornadas as $key => $jornada) {
            if (isset($jornada['inicio'])) {
                $jornada_formatada[$key]['inicio'] = date("d/m/y H:i", strtotime($jornada['inicio']['timestamp_transmissao']));
            }
            if (isset($jornada['termino'])) {
                $jornada_formatada[$key]['termino'] = date("d/m/y H:i", strtotime($jornada['termino']['timestamp_transmissao']));
            }

            if (isset($jornada['inicio']) && isset($jornada['termino'])) {
                $jornada_formatada[$key]['total_horas'] = round($jornada['termino']['horimetro'] - $jornada['inicio']['horimetro'], 3);
                if ($jornada_formatada[$key]['total_horas'] < 24 && $jornada_formatada[$key]['total_horas'] > 0)
                    $somatorio_horas += $jornada_formatada[$key]['total_horas'];
            }
        }

        $jornada_formatada['total_horas'] = $somatorio_horas;
        $jornada_formatada['nome_condutor'] = $nome_condutor;

        $resposta = json_encode($jornada_formatada);

        $this->load->view("ajax/ajax_resposta", array("resposta" => $resposta));
    }

}
