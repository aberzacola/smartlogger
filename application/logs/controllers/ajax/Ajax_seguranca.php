<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajax_transmissoes
 *
 * @author Angelo
 */
class Ajax_seguranca extends CI_Controller {

    //put your code here

    public function efetuar_login() {
        $this->load->model("Seguranca_model", "", TRUE);

        //Recebendo os dados que recebo via post
        $email = $this->input->post("email", TRUE);
        $senha = $this->input->post("senha", TRUE);

        //Criptografando senha
        $senha_md5 = md5($senha);

        $login_valido = $this->Seguranca_model->verifica_usuario($email, $senha_md5);
        
        //Somente se for válido registra as sessions neccessárias
        if ($login_valido == false) {
            $resposta['status'] = false;
            $data['resposta'] = json_encode($resposta);

            $this->load->view("ajax/ajax_resposta", $data);
            return;
        }
        $_SESSION['id_usuario'] = $login_valido['id'];
        $_SESSION['nome_usuario'] = $login_valido['nome'];
        $_SESSION['id_empresa'] = $login_valido['id_empresa'];

        $resposta['status'] = true;
        $resposta['nome'] = $login_valido['nome'];

        $data['resposta'] = json_encode($resposta);

        $this->load->view("ajax/ajax_resposta", $data);
    }

}
