<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_cerca extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function rota_entre_trans() {

        $this->load->model("Rotas_model", "", TRUE);


        $id_trans_inicio = $this->input->post("id_trans_inicio", TRUE);
        $id_trans_final = $this->input->post("id_trans_final", TRUE);

        $resposta = $this->Rotas_model->get_intervalo_trans($id_trans_inicio, $id_trans_final);

        $data['resposta'] = json_encode($resposta);

        $this->load->view("ajax/ajax_resposta", $data);
    }

    public function get_cerca() {

        $this->load->model("Cerca_model", "", TRUE);
        
        $this->load->library("Calculo_cerca");

        $id_cerca = $this->input->post("id_cerca", true);

        $cerca_data = $this->Cerca_model->get_cerca($id_cerca);

        $cerca_data['poligono'] = $this->calculo_cerca->processa_poligono($cerca_data['poligono']);

        $resposta = json_encode($cerca_data);

        $this->load->view("ajax/ajax_resposta", array("resposta" => $resposta));
    }

    public function deleta_cerca() {
        

        $this->load->model("Cerca_model", "", true);

        $id_cerca = $this->input->post("id_cerca", TRUE);

        $resposta = $this->Cerca_model->deleta_cerca($id_cerca);
        
        $resposta = 1;

        $this->load->view("ajax/ajax_resposta", array("resposta" => $resposta));
    }

    public function cadastra_cerca() {

        $this->load->model("Cerca_model", "", TRUE);
        $poligon_json = $this->input->post("poligono");
        $nome = $this->input->post("nome_cerca");
        $zoom = $this->input->post("zoom");

        $poligono = json_decode($poligon_json);

        $resposta = $this->Cerca_model->set_cerca($nome, $_SESSION['id_empresa'], $poligono, $zoom);

        $this->load->view("ajax/ajax_resposta", array("resposta" => $resposta));
    }

}
