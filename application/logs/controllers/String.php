<?php

session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class String extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function insere_string($string_gsm = "ST300STT;205035087;04;378;20151026;13:00:01;40708;-19.946448;-044.014269;000.017;000.00;10;1;932156;13.23;000000;1;2160;008420;4.1;1;0;01D5981F170000;0") {


        $this->load->model("string_model", "", TRUE);
        $this->load->model("cerca_model", "", TRUE);
        $this->load->model("alertas_model", "", TRUE);
        $this->load->library("calculo_cerca");

        $dados = explode(';', $string_gsm);

// YYYY-MM-DD HH:MM:SS
        $date_time = substr($dados[4], 0, 4) . "-" . substr($dados[4], 4, 2) . "-" . substr($dados[4], 6, 2) . " " . $dados[5];

        $date = new DateTime($date_time);
        $timestamp = $date->getTimestamp();
        $timestamp -= 60 * 60 * 3;
        $date->setTimestamp($timestamp);

        $dados_trad['header'] = $dados[0];
        $dados_trad['device_id'] = $dados[1];
        $dados_trad['model'] = $dados[2];
        $dados_trad['software_version'] = $dados[3];
        $dados_trad['date_time'] = $date->format('Y-m-d H:i:s');
        $dados_trad['cell_location'] = $dados[6];
        $dados_trad['latitude'] = $dados[7];
        $dados_trad['longitude'] = $dados[8];
        $dados_trad['velocidade'] = $dados[9];
        $dados_trad['course_on_ground'] = $dados[10];
        $dados_trad['num_satelites'] = $dados[11];
        $dados_trad['gps_fixado'] = $dados[12];
        $dados_trad['hodometro'] = $dados[13];
        $dados_trad['tensao_main_bat'] = $dados[14];
        $dados_trad['status_io'] = $dados[15];
        $dados_trad['parado_dirigindo'] = $dados[16];
        $dados_trad['msg_number'] = $dados[17];
        $dados_trad['horimetro'] = $dados[18];
        $dados_trad['tensao_backup_bat'] = $dados[19];
        $dados_trad['em_tempo_real'] = $dados[20];
        $dados_trad['rpm'] = $dados[21];
        $dados_trad['driver_id'] = $dados[22];
        $dados_trad['driver_id_reg'] = $dados[23];

// Inserindo String no banco
        $id_transmissao = $this->string_model->insert_string($dados_trad);

// Verificando se a máquina está fora ou dentro da cerca
        $dados_marcara = $this->string_model->get_mascara_data($dados_trad['device_id']);

        $cercas = $this->cerca_model->get_cercas($dados_marcara['emp_id']);

        foreach ($cercas as $cerca) {
            $cerca_data = $this->cerca_model->get_cerca($cerca['id']);

            $cerca_data['poligono'] = str_ireplace("POLYGON((", "", $cerca_data['poligono']);
            $cerca_data['poligono'] = str_ireplace("))", "", $cerca_data['poligono']);
            $cerca_data['poligono'] = explode(",", $cerca_data['poligono']);

            foreach ($cerca_data['poligono'] as $key => $ponto) {
                $cerca_data['poligono'][$key] = explode(" ", $ponto);
            }

            $dentro = $this->calculo_cerca->dentro_cerca(array($dados_trad['latitude'], $dados_trad['longitude']), $cerca_data['poligono']);
            $ultimo_alarme = $this->alertas_model->ultimo_alerta($dados_trad['device_id']);
            

            if ($dentro === false) {                    
                if ($ultimo_alarme['tipo_alarme'] == "dentro_cerca" || $ultimo_alarme == false) {
                    $entrada = array("id" => NULL,
                        "id_router_gsm" => $dados_trad['device_id'],
                        "tipo_alarme" => 'fora_cerca',
                        "id_cerca" => $cerca['id'],
                        "id_transmissao" => $id_transmissao,
                        "date_time" => $dados_trad['date_time'],
                        "latitude" => $dados_trad['latitude'],
                        "longitude" => $dados_trad['longitude']);
                    $this->alertas_model->insert_alerta($entrada);
                }
            } else {
                if ($ultimo_alarme['tipo_alarme'] == "fora_cerca") {
                    $entrada = array("id" => NULL,
                        "id_router_gsm" => $dados_trad['device_id'],
                        "tipo_alarme" => 'dentro_cerca',
                        "id_cerca" => $cerca['id'],
                        "id_transmissao" => $id_transmissao,
                        "date_time" => $dados_trad['date_time'],
                        "latitude" => $dados_trad['latitude'],
                        "longitude" => $dados_trad['longitude']);
                    $this->alertas_model->insert_alerta($entrada);
                }
            }
        }
// preenchenco a primeira parte, com dados de horimetro
        $horimetro = dechex($dados_trad['horimetro']);

// preenchendo o espaço vazio com 0 a esquerda
        $string_antiga[0] = str_pad($horimetro, 8, "0", STR_PAD_LEFT);

// transformando em HEX para colar na string
        $id_router = dechex($dados_marcara['id_router']);

// Velocidade de pico é zerada
        $velocidade_pico = "0000";
        $string_antiga[1] = $velocidade_pico . $id_router;

// Temperatura de pico zerada
        $temp_pico = "0000";
        $string_antiga[2] = $temp_pico;

// Preenchendo hodometro mais significativo e menos significativo
        $hodometro = dechex($dados_trad['hodometro']);
        $hodometro = str_pad($hodometro, 8, "0", STR_PAD_LEFT);

        $hodometro_mais_sig = substr($hodometro, 0, 4);
        $string_antiga[3] = $hodometro_mais_sig;

        $hodometro_menos_sig = substr($hodometro, -4);
        $string_antiga[9] = $hodometro_menos_sig;

// Criando o Objeto date time para ser usado na hora de setar dias , meses e anos
        $date = new DateTime($dados_trad['date_time']);

// Dia segundo GPS
        $dia = $date->format('d');
        $string_antiga[4] = $dia;

// Mes segundo GPS
        $mes = $date->format('m');
        $string_antiga[5] = $mes;

// Colocando AnoHora , ano os dois primeiros tipo, 2010 vira 10 e horas as duas ultimas.
        $ano_hora = $date->format('y') . $date->format('H');
        $string_antiga[6] = $ano_hora;

// Colocando MinutoSegundo, minutos primeiros, segundos por ultimo.
        $minuto_segundo = $date->format('i') . $date->format('s');
        $string_antiga[8] = $minuto_segundo;

//Tratamento do numero da base
        $string_antiga[7] = '0000';

// Rede em que a base está cadastrada
        $rede = str_pad($dados_marcara['rede'], 4, "0", STR_PAD_LEFT);
        $string_antiga[10] = $rede;

        $string_antiga[11] = 0;

// Colocando a latitude
        $string_antiga[12] = $dados_trad['latitude'];

// Orientação Latitude
        $string_antiga[13] = 'LAT'; // Pra nao ter que transformar no jeito que o router envia, já identifico aqui e no ID eu pulo uma etapa e trato corretamente
// Colocando a longitude
        $string_antiga[14] = $dados_trad['longitude'];

// Orientação Latitude
        $string_antiga[15] = 'LON'; // Pra nao ter que transformar no jeito que o router envia, já identifico aqui e no ID eu pulo uma etapa e trato corretamente
// Hora Modem, nao usa, vai zerado
        $string_antiga[16] = 000000;

// validação do GPS
        if ($dados_trad['gps_fixado'] == 1)
            $string_antiga[17] = "A";
        else
            $string_antiga[17] = "V";

// Temp modem
        $string_antiga[18] = 0;

// Dado hrt
        $string_antiga[19] = 0;

// organizando array pela ordem das keys
        ksort($string_antiga);

        $string_antiga_linear = "";
        foreach ($string_antiga as $key => $value)
            $string_antiga_linear .= $value . ",";
        $string_antiga_linear = substr($string_antiga_linear, 0, -1);

        $handle = file_get_contents("http://injetec.com.br/horimetro/id.php?h=$string_antiga_linear", 'r');
    }

}
