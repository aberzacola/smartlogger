<?php

session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Checklist extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function criar_checklist() {

        $data['id_empresa'] = $_SESSION['id_empresa'];

        $this->load->model("checklist_model", "", TRUE);

        //Pega o utlimo checklist cadastrado
        $check_list = $this->checklist_model->get_last_checklist($data['id_empresa']);
       

        if ($check_list != FALSE) {

            $itens_criticos = unserialize($check_list['itens_criticos']);

            //Organizando itens criticos para exibir na text area
            $check_list['itens_criticos'] = "";
            foreach ($itens_criticos as $itens) {
                $check_list['itens_criticos'] .= trim($itens) . "\n";
            }

            $itens_gerais = unserialize($check_list['itens_gerais']);

            //Organizando itens gerais para exibir na text area
            $check_list['itens_gerais'] = "";
            foreach ($itens_gerais as $itens) {
                $check_list['itens_gerais'] .= trim($itens) . "\n";
            }
            $data['checklist'] = $check_list;
        } else {
            //Checklist padrão para itens criticos
            $itens_criticos = array(
                "Faróis / Lanterna / Giroflex",
                "Luz de freio / ré",
                "Extintor de Incêndio",
                "Funcionamento do sistema de elevação e inclinação",
                "Buzina",
                "Linha de vida dos pneus",
                "Sistemas de direção",
                "Sinal sonoro de marcha ré",
                "Funcionamento dos freios",
                "Funcionamento do freio de mão",
                "Sistema de partida do motor",
                "Vazamento de combustível / GLP",
                "Travamentos dos cilindros de GLP",
                "Vazamento de óleo",
                "Cinto de segurança"
            );
            
            // Gerando uma string com os itens
            $check_list['itens_criticos'] = "";
            foreach ($itens_criticos as $itens) {
                $check_list['itens_criticos'] .= trim($itens) . "\n";
            }
            
            //Removendo o ultimo \n
            $check_list['itens_criticos'] = rtrim($check_list['itens_criticos'], "\n");

            //Checklist padrão para itens gerais
            $itens_gerais = array(
                "Número de identificação( TAG / Logotipo da empresa )",
                "Funcionamento da trava dos garfos",
                "Espelhos retrovisores",
                "Sinal sonoro do limitador de velocidade",
                "Estado dos garfos",
                "Identificação da capacidade de carga",
                "Assento",
                "Instrumentos de painel",
                "Organização geral( papel / plástico dentro da máquina",
                "Estado das correntes / torre de eleveção"
            );
            
            // Gerando uma string com os itens
            $check_list['itens_gerais'] = "";
            foreach ($itens_gerais as $itens) {
                $check_list['itens_gerais'] .= trim($itens) . "\n";
            }
            
            //Removendo o ultimo \n
            $check_list['itens_gerais'] = rtrim($check_list['itens_gerais'], "\n");

            //Inputando data para imprimir no json view
            $data['checklist'] = $check_list;
        }



        $this->load->view("criar_checklist", $data);
    }
    
    public function historico_checklist() {
        
        $data['id_empresa'] = $_SESSION['id_empresa'];

        $this->load->model("checklist_model", "", TRUE);
        $this->load->model("maquina_model", "", TRUE);

        $data['maquinas'] = $this->maquina_model->get_all_maquinas($_SESSION['id_empresa']);
        
        $this->load->view("historico_checklist", $data);
    }

}
