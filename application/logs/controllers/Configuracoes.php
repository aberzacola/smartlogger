<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracoes extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function posicao_padrao() {

        $data['id_empresa'] = $_SESSION['id_empresa'];
        ;

        $this->load->model("Configs_model", "", TRUE);

        $resposta = $this->Configs_model->get_cadastro_empresa($data['id_empresa']);

        $data['dados_empresa'] = $resposta[0];


        $this->load->view('posicao_padrao', $data);
    }

    public function cadastra_usuario() {

        $this->load->view("config_cadastra_usuario");
    }

    public function lista_usuarios() {

        $this->load->model("Configs_model", "", TRUE);

        $data['usuarios_cadastrados'] = $this->Configs_model->usuarios_cadastrados($_SESSION['id_empresa']);

        $this->load->view("config_lista_usuarios", $data);
    }

}
