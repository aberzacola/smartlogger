<?php

session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Seguranca extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function login() {
        $this->load->model("Seguranca_model", "", TRUE);


        $this->load->view("login");
    }

    public function logout() {
        session_destroy();
        header("Location: /seguranca/login");
        die();
    }
    
    public function cadastrar_usuario(){
        
    }

}
