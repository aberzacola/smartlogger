<?php

session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Alertas extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function alertas_cerca_lista() {
        $this->load->model("Alertas_model", "", TRUE);
        $this->load->model("Maquina_model", "", TRUE);
        $this->load->model("Condutores_model", "", TRUE);
        $this->load->model("Cerca_model", "", TRUE);
        $this->load->model("Servico_model", "", TRUE);
        $this->load->library("Calculos_maquinas");
//        $this->output->enable_profiler(TRUE);
//        
        //Pega ID da empresa cadastrado no sistema antigo
        $data['id_empresa'] = $_SESSION['id_empresa'];

        $maquinas = $this->Maquina_model->get_all_maquinas($data['id_empresa']);
        $data['maquinas'] = $maquinas;

        $id_maquina = $this->input->post('id_maquina', TRUE);

        if (is_null($id_maquina)) {
            foreach ($maquinas as $maquina) {
                $alertas[$maquina['id']]['alertas'] = $this->Alertas_model->get_todos_alertas_maquina($maquina['id']);
                if (!isset($alertas[$maquina['id']]['dados_maquina'])) {
                    $alertas[$maquina['id']]['dados_maquina'] = $maquina;
                }
                $maquinas_formated[$maquina['id']] = $maquina;
            }
        } else {
            $dataInicial = $this->input->post("dataInicialSistema", TRUE);
            $dataFinal = $this->input->post("dataFinalSistema", TRUE);

            $data['dataInicio'] = $this->input->post("dataInicial", TRUE);
            $data['dataFinal'] = $this->input->post("dataFinal", TRUE);

            $trans_between = $this->Maquina_model->get_all_trans_between($id_maquina, $dataInicial, $dataFinal);
            if (count($trans_between) > 0) {
                $primeira_transmissao_id = $trans_between[0]['id'];
                $ultima_transmissao_id = $trans_between[count($trans_between) - 1]['id'];

                foreach ($maquinas as $maquina) {
                    if ($maquina['id'] != $id_maquina) {
                        continue;
                    }
                    $alertas[$id_maquina]['alertas'] = $this->Alertas_model->get_todos_alertas_maquina_between($id_maquina, $primeira_transmissao_id, $ultima_transmissao_id);
                    if (!isset($alertas[$maquina['id']]['dados_maquina'])) {
                        $alertas[$maquina['id']]['dados_maquina'] = $maquina;
                        $data['nomeMaquina'] = $maquina['nome'];
                    }
                    $maquinas_formated[$maquina['id']] = $maquina;
                }
            }
        }



        foreach ($alertas as $maquina_id => $dados_maquina_e_alertas) {
            if (!empty($dados_maquina_e_alertas['alertas'])) {
                foreach ($dados_maquina_e_alertas['alertas'] as $key => $alertas_maquina) {
                    if (!isset($indice_array_id_cerca[$alertas_maquina['id_cerca']])) {
                        $indice_array_id_cerca[$alertas_maquina['id_cerca']] = 0;
                    } else {
                        $indice_array_id_cerca[$alertas_maquina['id_cerca']] ++;
                    }
                    $transmissao = $this->Servico_model->get_transmissao($alertas_maquina['id_transmissao']);
                    $alertas_formated[$maquina_id]
                            ['alertas']
                            [$alertas_maquina['id_cerca']]
                            [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                            ['info_transmissao'] = $transmissao;
                    if ($transmissao['id_condutor'] != -1) {
                        $nome_condutor = $this->Condutores_model->get_condutor_nome($transmissao['id_condutor']);
                        $alertas_formated[$maquina_id]
                                ['alertas']
                                [$alertas_maquina['id_cerca']]
                                [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                                ['info_condutor']['nome'] = empty($nome_condutor) ? "Sem condutor" : current($nome_condutor);
                    } else {
                        $alertas_formated[$maquina_id]
                                ['alertas']
                                [$alertas_maquina['id_cerca']]
                                [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                                ['info_condutor']['nome'] = "Sem condutor";
                    }
                    $alertas_formated[$maquina_id]
                            ['alertas']
                            [$alertas_maquina['id_cerca']]
                            [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                            ['info_transmissao']
                            ['hora_transmissao_formatada'] = $this->calculos_maquinas->formata_data_atualizacao($alertas_formated[$maquina_id]['alertas'][$alertas_maquina['id_cerca']][$indice_array_id_cerca[$alertas_maquina['id_cerca']]]['info_transmissao']['timestamp_transmissao']);
                    $alertas_formated[$maquina_id]
                            ['alertas']
                            [$alertas_maquina['id_cerca']]
                            [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                            ['info_transmissao']
                            ['hora_transmissao_timestamp'] = $this->calculos_maquinas->formata_data_atualizacao_to_timestamp($alertas_formated[$maquina_id]['alertas'][$alertas_maquina['id_cerca']][$indice_array_id_cerca[$alertas_maquina['id_cerca']]]['info_transmissao']['timestamp_transmissao']);
                    $alertas_formated[$maquina_id]
                            ['alertas']
                            [$alertas_maquina['id_cerca']]
                            [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                            ['info_alerta'] = $alertas_maquina;
                    $cerca_data = $this->Cerca_model->get_cerca_nome($alertas_maquina['id_cerca']);
                    $alertas_formated[$maquina_id]
                            ['alertas']
                            [$alertas_maquina['id_cerca']]
                            [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                            ['info_alerta']['nome_cerca'] = $cerca_data['nome_cerca'];
                }
            }

            $indice_array_id_cerca = array();
        }
        $id = 0;
        if (isset($alertas_formated)) {
            foreach ($alertas_formated as $id_maquina => $maquinas_alerta) {
                foreach ($maquinas_alerta as $alertas) {
                    foreach ($alertas as $cercas) {

                        foreach ($cercas as $id => $eventos) {
                            $tabela[$eventos['info_transmissao']['hora_transmissao_timestamp']]['hora_evento'] = $eventos['info_transmissao']['hora_transmissao_formatada'];
                            $tabela[$eventos['info_transmissao']['hora_transmissao_timestamp']]['lat'] = $eventos['info_transmissao']['lat'];
                            $tabela[$eventos['info_transmissao']['hora_transmissao_timestamp']]['lng'] = $eventos['info_transmissao']['lng'];
                            $tabela[$eventos['info_transmissao']['hora_transmissao_timestamp']]['nome_cerca'] = $eventos['info_alerta']['nome_cerca'];
                            $tabela[$eventos['info_transmissao']['hora_transmissao_timestamp']]['tipo_evento'] = $eventos['info_alerta']['dentro_cerca'] == 0 ? "Fora Cerca" : "Dentro Cerca";
                            $tabela[$eventos['info_transmissao']['hora_transmissao_timestamp']]['nome_maquina'] = $maquinas_formated[$eventos['info_alerta']['id_maquina']]['nome'];
                            $tabela[$eventos['info_transmissao']['hora_transmissao_timestamp']]['tipo_evento'] = $eventos['info_alerta']['dentro_cerca'] == 0 ? "Fora Cerca" : "Dentro Cerca";
                            $tabela[$eventos['info_transmissao']['hora_transmissao_timestamp']]['id_cerca'] = $eventos['info_alerta']['id_cerca'];
                            $tabela[$eventos['info_transmissao']['hora_transmissao_timestamp']]['nome_condutor'] = $eventos['info_condutor']['nome'];
                            $id++;
                        }
                    }
                }


                $id ++;
            }
            krsort($tabela);
            $data['todos_alertas'] = $tabela;
        } else {
            $data['todos_alertas'] = false;
        }





        $this->load->view("alerta_lista", $data);
    }

    public function alertas_cerca_lista_depois_faco() {
        $this->load->model("Alertas_model", "", TRUE);
        $this->load->model("Maquina_model", "", TRUE);
        $this->load->model("Condutores_model", "", TRUE);
        $this->load->model("Cerca_model", "", TRUE);
        $this->load->model("Servico_model", "", TRUE);
        $this->load->library("Calculos_maquinas");
//        $this->output->enable_profiler(TRUE);
//        
        //Pega ID da empresa cadastrado no sistema antigo
        $data['id_empresa'] = $_SESSION['id_empresa'];

        $maquinas = $this->Maquina_model->get_all_maquinas($data['id_empresa']);

        foreach ($maquinas as $maquina) {
            $alertas[$maquina['id']]['alertas'] = $this->Alertas_model->get_todos_alertas_maquina($maquina['id']);
            if (!isset($alertas[$maquina['id']]['dados_maquina'])) {
                $alertas[$maquina['id']]['dados_maquina'] = $maquina;
            }
        }

        foreach ($alertas as $maquina_id => $dados_maquina_e_alertas) {
            if (!empty($dados_maquina_e_alertas['alertas'])) {
                foreach ($dados_maquina_e_alertas['alertas'] as $key => $alertas_maquina) {
                    if (!isset($indice_array_id_cerca[$alertas_maquina['id_cerca']])) {
                        $indice_array_id_cerca[$alertas_maquina['id_cerca']] = 0;
                    } else {
                        $indice_array_id_cerca[$alertas_maquina['id_cerca']] ++;
                    }
                    $alertas_formated[$maquina_id]
                            ['alertas']
                            [$alertas_maquina['id_cerca']]
                            [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                            ['info_transmissao'] = $this->Servico_model->get_transmissao($alertas_maquina['id_transmissao']);
                    $alertas_formated[$maquina_id]
                            ['alertas']
                            [$alertas_maquina['id_cerca']]
                            [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                            ['info_transmissao']
                            ['hora_transmissao_formatada'] = $this->calculos_maquinas->formata_data_atualizacao($alertas_formated[$maquina_id]['alertas'][$alertas_maquina['id_cerca']][$indice_array_id_cerca[$alertas_maquina['id_cerca']]]['info_transmissao']['timestamp_transmissao']);
                    $alertas_formated[$maquina_id]
                            ['alertas']
                            [$alertas_maquina['id_cerca']]
                            [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                            ['info_alerta'] = $alertas_maquina;
                    $cerca_data = $this->Cerca_model->get_cerca_nome($alertas_maquina['id_cerca']);
                    $alertas_formated[$maquina_id]
                            ['alertas']
                            [$alertas_maquina['id_cerca']]
                            [$indice_array_id_cerca[$alertas_maquina['id_cerca']]]
                            ['info_alerta']['nome_cerca'] = $cerca_data['nome_cerca'];
                }
            }
            $indice_array_id_cerca = array();
        }
        $id = 0;
        foreach ($alertas_formated as $maquinas) {
            foreach ($maquinas as $alertas) {
                foreach ($alertas as $cercas) {
                    foreach ($cercas as $id => $eventos) {
                        $tabela[$id]['hora_evento'] = $eventos['info_transmissao']['hora_transmissao_formatada'];
                        $id++;
                    }
                }
            }


            $id ++;
        }

        $data['todos_alertas'] = $tabela;

        echo "<pre>";
        print_r($alertas_formated);
        $this->load->view("alerta_lista", $data);
    }

}
