<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manutencao extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function preventiva_cadastro() {

        $this->load->model("Maquina_model", "", TRUE);

        $id_empresa = $_SESSION['id_empresa'];

        //Todas as máquinas da empresa 
        $data['maquinas'] = $this->Maquina_model->get_all_maquinas($id_empresa);

        $data['id_maquina'] = 0;

        $this->load->view('manutencao_preventiva_cadastro', $data);
    }

    public function preventiva_cadatro_detalhe($id_maquina = 0) {

        $this->load->model("Maquina_model", "", TRUE);
        $this->load->model("Manutencao_model", "", TRUE);

        $id_empresa = $_SESSION['id_empresa'];

        //Todas as máquinas da empresa 
        $data['maquinas'] = $this->Maquina_model->get_all_maquinas($id_empresa);

        if ($id_maquina != 0) {
            //Pega a ultima transmissao da maquian para pegar o horimetro
            $last_transmission = $this->Maquina_model->get_last_transmission($id_maquina);

            //Pega as infos da tabela de manutencao
            $pecas = $this->Manutencao_model->get_pecas_maquina($id_maquina);

            foreach ($pecas as $key => $peca) {
                $pecas[$key]['tempo_restante'] = $last_transmission[0]['horimetro'];
            }

            $data['pecas'] = $pecas;
        }



        //Detalhes
        $data['id_maquina'] = $id_maquina;

        $this->load->view('manutencao_preventiva_cadastro', $data);
    }

}
