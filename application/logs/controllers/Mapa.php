<?php

session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapa extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    
    public function plotar_todas_maq() {

        $data['id_empresa'] = $_SESSION['id_empresa'];
        
        $this->load->model("Configs_model", "", TRUE);

        $resposta = $this->Configs_model->get_cadastro_empresa($_SESSION['id_empresa']);

        $data['dados_empresa'] = $resposta[0];

        $this->load->view("plotar_todas", $data);
    }

    public function mapa_cria_cerca() {

        $this->load->model("Configs_model", "", TRUE);

        $data['id_empresa'] = $_SESSION['id_empresa'];

        $resposta = $this->Configs_model->get_cadastro_empresa($_SESSION['id_empresa']);

        $data['dados_empresa'] = $resposta[0];

        $this->load->view('mapa_cria_cerca', $data);
    }

    public function mapa_lista_cerca() {


        $this->load->model("Cerca_model", "", true);


        $cercas = $this->Cerca_model->get_cercas($_SESSION['id_empresa']);

        $data['cercas'] = $cercas;

        $data['id_empresa'] = $_SESSION['id_empresa'];

        $this->load->view('mapa_lista_cerca', $data);
    }

}
