<?php

session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function transmissao() {
        $this->load->model("Servico_model", "", TRUE);
        $this->load->model("Cerca_model", "", TRUE);
        $this->load->model("Alertas_model", "", TRUE);
        $this->load->model("Maquina_model", "", TRUE);
        $this->load->model("Dados_model", "", TRUE);
        $this->load->library("Calculo_cerca");

        //Recebe o input RAW$dados_transmissao_json
        $dados_transmissao_json = file_get_contents("php://input");

//        $dados_transmissao_json = '{"id_maquina":104,"tb_operador":[{"id_operador":100,"timestamp":1497982141}],"tb_transmissao":[{"timestamp":1497982141,"gps":{"lat":-19.9436985,"lng":-44.00846076,"velocidade":0,"altitude":926,"precisao":11,"bearing":0},"acelerometro":{"total":0.36803781933341356,"x":0,"y":0,"z":0},"horimetro":0.4000000059604645,"temperatura":{"temperatura_instantanea":1,"temperatura_pico":222},"velocidade_pcb":{"velocidade_instantanea":0,"velocidade_maxima":0,"velocidade_media":0},"tempo_maquina_movimento":0,"id_operador":100,"luz_marcha_re":"1","valor_ad_ref_1v":4,"pulsos_em_200ms":0,"status_rele":{"rele":"1","motivo_status_rele":"Rel%C3%A9+habilitado+por+operador"},"bateria":{"nivel":92,"carregando":false}}],"tb_on_off":[{"estado":"0","timestamp":1497982141}],"tb_cinto_on_off":[{"id_operador":100,"estado":"0","timestamp":1497982141}],"tb_checklist":[{"versao":0,"criticos":{"Angelo":"Sim","Far%C3%B3is+%2F+Lanterna+%2F+Giroflex":"Sim","Luz+de+freio+%2F+r%C3%A9":"Sim","Extintor+de+Inc%C3%AAndio":"Sim","Funcionamento+do+sistema+de+eleva%C3%A7%C3%A3o+e+inclina%C3%A7%C3%A3o":"Nao","Buzina":"Nao","Linha+de+vida+dos+pneus":"Nao","Sistemas+de+dire%C3%A7%C3%A3o":"Nao","Sinal+sonoro+de+marcha+r%C3%A9":"Nao","Funcionamento+dos+freios":"Nao"},"geral":{"Garfos":"Sim","Espelho Retrovisor":"Nao","Assento":"NA"}}]}';
        file_put_contents($_SERVER["HTTP_ID"] . "_json.txt", $dados_transmissao_json);
        //Tipo de dado enviado tem que ser json
        if (!isset($_SERVER["CONTENT_TYPE"]) || $_SERVER["CONTENT_TYPE"] != "application/json") {
            $this->output->set_status_header(400);
            return;
        }

        //Verificando o ID e senha da maquina que está fazendo transmissão
        $maquina_valida = false;
        if (isset($_SERVER["HTTP_ID"]) && isset($_SERVER["HTTP_SENHA"])) {
            $maquina_valida = $this->Servico_model->valida_transmissao($_SERVER["HTTP_ID"], $_SERVER["HTTP_SENHA"]);
        }

        // Se a máquina não efetuar um login válido, emitido um erro 403 , acesso proibido
        if ($maquina_valida == false) {
            $this->output->set_status_header(403);
            return;
        }

        //id da maquina pego no cabeçalho
        $id_maquina = $_SERVER["HTTP_ID"];
//        $id_maquina = 100;
        //Ultima versão de checklist recebido
        $versao_check_list = 0;

        //Transformando o JSON em ARRAY
        $dados_transmissao_array = json_decode($dados_transmissao_json, TRUE);

        //Formantando a array de transmissão para entrar na tb_operador
        if (isset($dados_transmissao_array['tb_operador'])) {
            $tb_operador = $dados_transmissao_array['tb_operador'];
            foreach ($tb_operador as $key => $dados) {
                $insert_operador[$key]['id_maquina'] = $id_maquina;
                $insert_operador[$key]['id_condutor'] = $dados['id_operador'];
                $insert_operador[$key]['data_hora'] = date("Y-m-d H:i:s", $dados['timestamp']);
            }
            //Inserindo todas as transmissões de uma só vezno banco de dados, caso exista.
            if (isset($insert_operador)) {
                $this->Servico_model->inserir_array_operador($insert_operador);
            }
        }

        //Formantando a array da transmissão, pra ficar do jeito que o Codeigniter envia tudo para o banco de uma vez
        if (isset($dados_transmissao_array['tb_transmissao'])) {
            //Dados para a tabela transmissao
            $tbl_transmissao = $dados_transmissao_array['tb_transmissao'];
            foreach ($tbl_transmissao as $key => $transmissao) {
                $insert_transmissao[$key]['id_maquina'] = $id_maquina;
                $insert_transmissao[$key]['id_condutor'] = $transmissao['id_operador'];
                $insert_transmissao[$key]['timestamp_transmissao'] = date("Y-m-d H:i:s", $transmissao['timestamp']);
                $insert_transmissao[$key]['lat'] = $transmissao['gps']['lat'];
                $insert_transmissao[$key]['lng'] = $transmissao['gps']['lng'];
                $insert_transmissao[$key]['velocidade'] = $transmissao['velocidade_pcb']['velocidade_instantanea'];
                $insert_transmissao[$key]['velocidade_pico'] = $transmissao['velocidade_pcb']['velocidade_maxima'];
                $insert_transmissao[$key]['velocidade_media'] = $transmissao['velocidade_pcb']['velocidade_media'];
                $insert_transmissao[$key]['altitude'] = $transmissao['gps']['altitude'];
                $insert_transmissao[$key]['precisao'] = $transmissao['gps']['precisao'];
                $insert_transmissao[$key]['acelerometro'] = $transmissao['acelerometro']['total'];
                $insert_transmissao[$key]['horimetro'] = $transmissao['horimetro'];
                $insert_transmissao[$key]['hodometro'] = $transmissao['hodometro']['hodometro'];
                $insert_transmissao[$key]['hodometro_parcial'] = $transmissao['hodometro']['hodometro_parcial'];
                $insert_transmissao[$key]['hora_efetiva'] = $transmissao['hora_efetiva'];
                $insert_transmissao[$key]['temperatura_instantanea'] = $transmissao['temperatura']['temperatura_instantanea'];
                $insert_transmissao[$key]['temperatura_pico'] = $transmissao['temperatura']['temperatura_pico'];
                $insert_transmissao[$key]['tempo_maquina_movimento'] = $transmissao['tempo_maquina_movimento'];
                $insert_transmissao[$key]['bateria_nivel'] = $transmissao['bateria']['nivel'];
                $insert_transmissao[$key]['carregando'] = $transmissao['bateria']['carregando'] ? 1 : 0;
                $insert_transmissao[$key]['status_rele'] = $transmissao['status_rele']['motivo_status_rele'];
                $insert_transmissao[$key]['abastecimento_qtd'] = $transmissao['abastecimento']['quantidade'];
                $insert_transmissao[$key]['abastecimento_timestamp'] = $transmissao['abastecimento']['timestamp'];
                $insert_transmissao[$key]['motivo_rele'] = urldecode($transmissao['status_rele']['motivo_status_rele']);
            }
            //Inserindo todas as transmissões de uma só vezno banco de dados, caso exista.
            if (isset($insert_transmissao)) {
                $id_transmissao = $this->Servico_model->inserir_array_transmissoes($insert_transmissao);
            }
        }

        //Formatando a array de transmissao para entrar tb_on_off
        if (isset($dados_transmissao_array['tb_on_off'])) {
            $tb_on_off = $dados_transmissao_array['tb_on_off'];
            foreach ($tb_on_off as $key => $dados) {
                $insert_on_off[$key]['id_maquina'] = $id_maquina;
                $insert_on_off[$key]['estado'] = $dados['estado'];
                $insert_on_off[$key]['data_hora'] = date("Y-m-d H:i:s", $dados['timestamp']);
            }
            //Inserindo todas as transmissões de uma só vezno banco de dados, caso exista.
            if (isset($insert_on_off)) {
                $this->Servico_model->inserir_array_on_off($insert_on_off);
            }
        }



        //Formatando a array de transmissao para entrar tb_cinto_on_off
        if (isset($dados_transmissao_array['tb_cinto_on_off'])) {
            $tb_cinto_on_off = $dados_transmissao_array['tb_cinto_on_off'];
            foreach ($tb_cinto_on_off as $key => $dados) {
                $insert_cinto_on_off[$key]['id_maquina'] = $id_maquina;
                $insert_cinto_on_off[$key]['id_operador'] = $dados['id_operador'];
                $insert_cinto_on_off[$key]['estado'] = $dados['estado'];
                $insert_cinto_on_off[$key]['data_hora'] = date("Y-m-d H:i:s", $dados['timestamp']);
            }
            //Inserindo todas as transmissões de uma só vezno banco de dados, caso exista.
            if (isset($insert_cinto_on_off)) {
                $this->Servico_model->inserir_array_cinto_on_off($insert_cinto_on_off);
            }
        }



        //Formatando a array de transmissao para entrar tb_checklist
        if (isset($dados_transmissao_array['tb_checklist'])) {
            $tb_checklist = $dados_transmissao_array['tb_checklist'];
            foreach ($tb_checklist as $key => $dados) {
                if(serialize($dados['geral']) == "a:0:{}"){
                    continue;
                }
                $insert_checklist[$key]['id_maquina'] = $id_maquina;
                $insert_checklist[$key]['versao'] = $dados['versao'];
                $insert_checklist[$key]['datetime'] = date("Y-m-d H:i:s", $dados_transmissao_array['tb_transmissao'][$key]['timestamp']);
                $insert_checklist[$key]['itens_criticos'] = serialize($dados['criticos']);
                $insert_checklist[$key]['itens_gerais'] = serialize($dados['geral']);

                if ($dados['versao'] > $versao_check_list) {
                    $versao_check_list = $dados['versao'];
                }
                
                
            }
            //Inserindo todas as transmissões de uma só vezno banco de dados, caso exista.
            if (isset($insert_checklist)) {
                $this->Servico_model->inserir_array_checklist($insert_checklist);
            }
        }

        //Pegando as cercas da empresa
        $dados_maquina = $this->Maquina_model->get_maquina($id_maquina);

        //Verificando a existencia de um checklist mais novo que o recebido
        $last_checklist = $this->Servico_model->get_last_checklist_version($dados_maquina['id_empresa']);

        // A versão é controlada pelo id do checklist
        if ($versao_check_list < $last_checklist['id']) {
            $json['checklist']['novo'] = TRUE;
            $json['checklist']['itens']['criticos'] = unserialize($last_checklist['itens_criticos']);
            $json['checklist']['itens']['geral'] = unserialize($last_checklist['itens_gerais']);
            $json['config']['versao_checklist'] = $last_checklist['id'];
        } else {
            $json['checklist']['novo'] = FALSE;
        }

        //Dizendo quando será feito o prox checklist
        $json['config']['prox_checklist'] = time() + 300;



        //Id empresa
        $id_empresa = $dados_maquina['id_empresa'];

        //Pegando as cercas desta empresa
        $cercas_empresa = $this->Cerca_model->get_cercas($id_empresa);

        foreach ($cercas_empresa as $cerca_data) {
            if (isset($insert_transmissao))
                foreach ($insert_transmissao as $transmissao) {
                    if ($transmissao['lat'] == 0 || $transmissao['lng'] == 0) {
                        break;
                    }
                    //Pega o ultimo tipo de alerta dessa máquina, pra saber se ela saiu ou entrou na certa
                    $ultimo_alerta = $this->Alertas_model->ultimo_alerta($id_maquina, $cerca_data['id']);

                    $dados_cercas = $this->Cerca_model->get_cerca($cerca_data['id']);
                    $poligono = $this->calculo_cerca->processa_poligono($dados_cercas['poligono']);
                    $dentro_cerca = $this->calculo_cerca->dentro_cerca(array($transmissao['lat'], $transmissao['lng']), $poligono);

                    $alerta['id_maquina'] = $id_maquina;
                    $alerta['id_transmissao'] = $id_transmissao;
                    $alerta['id_cerca'] = $cerca_data['id'];
                    $alerta['dentro_cerca'] = $dentro_cerca;
                    
                    $this->Alertas_model->insert_alerta($alerta);
                }
        }

        //Registrando o USO de dados
        if (isset($dados_transmissao_array['tb_data_usage'])) {
            if (isset($dados_transmissao_array['tb_transmissao'][0])) {
                foreach ($dados_transmissao_array['tb_data_usage'] as $key => $tb_data_usage) {
                    $insert_uso_dados[$key]['id_maquina'] = $id_maquina;
                    $insert_uso_dados[$key]['data'] = date("Y-m-d H:i:s", $dados_transmissao_array['tb_transmissao'][$key]['timestamp']);
                    $insert_uso_dados[$key]['bytes_tx'] = $tb_data_usage['data_usage_tx'];
                    $insert_uso_dados[$key]['bytes_rx'] = $tb_data_usage['data_usage_rx'];

                    $this->Dados_model->input_data($insert_uso_dados);
                }
            }
        }

        //fazendo json
        $resposta = json_encode($json);

        $this->load->view("ajax/ajax_resposta", array("resposta" => $resposta));
    }

}
