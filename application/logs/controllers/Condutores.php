<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Condutores extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function cadastro() {

        $data['id_empresa'] = $_SESSION['id_empresa'];

        $this->load->view('condutores_cadastro', $data);
    }

    public function lista() {

        $this->load->model("Condutores_model", "", TRUE);

        $id_empresa = $_SESSION['id_empresa'];

        //setando id da empresa
        $data['id_empresa'] = $id_empresa;

        //Pegando a lista de condutores
        $data['condutores'] = $this->Condutores_model->get_condutores_empresa($id_empresa);

        $this->load->view('condutores_lista', $data);
    }

    //Gera o histórico do operador
    public function historico() {

        $this->load->model("Condutores_model", "", true);

        $id_empresa = $_SESSION['id_empresa'];
        
        //Nome da pagina
        $data['nome_pagina'] = "Histórico de condutores";

        //setando id da empresa
        $data['id_empresa'] = $id_empresa;

        //Pegando a lista de condutores
        $data['condutores'] = $this->Condutores_model->get_condutores_empresa($id_empresa);

        
        $this->load->view('condutores_historico', $data);
    }

}
