<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rastreio extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function painel($id_router_gsm) {

        $this->load->model("Maquina_model", "", TRUE);


        // pegando o ultimo posicionamento da maquina
        $data['ultima_transmissao'] = $this->Maquina_model->get_last_transmission($id_router_gsm);

        //pegando os dados relativos a máquina gsm no sistema antigo, atraves da máscara criada na hora do cadastro
        $dados_mascara = $this->Maquina_model->get_mascara_data($id_router_gsm);

        //Pegando o nome da máquina cadastrada no sistema antigo
        $data['nome_maquina_sis_antigo'] = $this->Maquina_model->get_nome_maquina_sis_antigo($dados_mascara['id_router'], $dados_mascara['rede'], $dados_mascara['emp_id']);

        //transformando o horimetro(minutos) em horimetro(horas)
        $data['ultima_transmissao']['horimetro_horas'] = round($data['ultima_transmissao']['horimetro']/60,1);
        
        //transformando o formato de data MySQL para Formato Brasil
        $date_time = new DateTime($data['ultima_transmissao']['date_time']);
        $data['ultima_transmissao']['data_ultima_atualizacao'] = $date_time->format("d/m/y H:i:s");
        
        //Arredondando a velocidade
        $data['ultima_transmissao']['velocidade_arredondada'] = round($data['ultima_transmissao']['velocidade']);
        
        //Hodometro em Km
        //transformando o horimetro(minutos) em horimetro(horas)
        $data['ultima_transmissao']['hodometro_km'] = round($data['ultima_transmissao']['hodometro']/1000,2);


        $this->load->view('painel', $data);
    }
    
    public function teste(){
        $this->load->view('teste');
    }

}
