<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_configs extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function checa_existencia_cadstro($id_empresa = false) {

        $this->load->model("Configs_model", "", TRUE);

        if ($id_empresa === false)
            $id_empresa = $this->input->post("id_empresa", TRUE);

        $resposta = $this->Configs_model->checa_exitencia_empresa($id_empresa);

        //convertendo TRUE e FALSE para 1 e 0 
        $resposta = ($resposta == TRUE) ? 1 : 0;

        //fazendo json
        $resposta_json["resposta"] = json_encode($resposta);

        $this->load->view("ajax/ajax_resposta", $resposta_json);
    }

    public function check_existencia_email() {

        $this->load->model("configs_model", "", TRUE);

        $email = $this->input->post("email", TRUE);

        $resposta = $this->configs_model->does_this_emails_exists($email);

        //fazendo json
        $resposta_json["resposta"] = json_encode($resposta);

        $this->load->view("ajax/ajax_resposta", $resposta_json);
    }

    public function cadastra_usuario_empresa() {

        $this->load->model("configs_model", "", TRUE);

        $id_empresa = $this->input->post("id_empresa", TRUE);
        $email = $this->input->post("email", TRUE);
        $nome = $this->input->post("nome", TRUE);
        $senha = md5($this->input->post("senha", TRUE));

        $resposta['id_usuario'] = $this->configs_model->cadastra_usuario($id_empresa, $nome, $email, $senha);
        
        //fazendo json
        $resposta_json["resposta"] = json_encode($resposta);

        $this->load->view("ajax/ajax_resposta", $resposta_json);
    }

    public function deleta_empresa() {

        $this->load->model("configs_model", "", TRUE);

        $id_empresa = $this->input->post("id_empresa", TRUE);

        $this->configs_model->deleta_empresa($id_empresa);

        $resposta = 1;

        $resposta_json["resposta"] = json_encode($resposta);

        $this->load->view("ajax/ajax_resposta", $resposta_json);
    }

    public function cadastra_ponto_padrao() {

        $this->load->model("Configs_model", "", TRUE);

        $id_empresa = $this->input->post("id_empresa", TRUE);
        $lat = $this->input->post("lat", TRUE);
        $lng = $this->input->post("lng", TRUE);
        $zoom = $this->input->post("zoom", TRUE);


        $this->Configs_model->set_ponto_padrao($id_empresa, $lat, $lng, $zoom);
    }

}
