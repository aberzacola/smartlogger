<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Empresas extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        if($_SESSION['global'] == 0){
            die();
        }
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function cadastro() {

        $this->load->view('admin/empresas_cadastro');
        
    }
    
    public function cadastro_usuario() {
        
        $this->load->model("Empresa_model", "", TRUE);
        
        $resposta['empresas'] = $this->Empresa_model->get_all_empresas();

        $this->load->view('/admin/usuario_cadastro',$resposta);
        
    }
    public function deleta_empresa() {
        
        $this->load->model("Empresa_model", "", TRUE);
        
        $resposta['empresas'] = $this->Empresa_model->get_all_empresas();

        $this->load->view('/admin/empresa_deletar',$resposta);
        
    }

}
