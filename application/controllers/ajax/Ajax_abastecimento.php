<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_abastecimento extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function abastecimento_maquina(){
        $this->load->model("abastecimento_model", "", TRUE);
        $this->load->model("maquina_model", "", TRUE);

        $data_inicio = $this->input->post("data_inicio", TRUE);
        $data_final = $this->input->post("data_final", TRUE);
        $id_maquina = $this->input->post("id_maquina", TRUE);
        
        $respostas = $this->abastecimento_model->get_abastecimentos_unica_maquina($data_inicio,$data_final,$id_maquina);
        $data['resposta'] = json_encode($respostas);
        $this->load->view("ajax/ajax_resposta", $data);
    }
    
    public function abastecimento_todas_maquinas() {

        $this->load->model("abastecimento_model", "", TRUE);
        $this->load->model("maquina_model", "", TRUE);

        $id_empresa = $_SESSION['id_empresa'];

        $data_inicio = $this->input->post("data_inicio", TRUE);
        $data_final = $this->input->post("data_final", TRUE);

        //Pegando todas as máquinas dessa empresa
        $maquinas_empresa = $this->maquina_model->get_all_maquinas($id_empresa);
        $maquinas_empresa_print = array();
        foreach ($maquinas_empresa as $maquina) {
            $ids_maquinas[] = $maquina['id'];
            $maquinas_empresa_print[$maquina['id']]['dados_maquina'] = $maquina;
        }


        $respostas = $this->abastecimento_model->get_intervalo_datas_trans_multi_id($data_inicio, $data_final, $ids_maquinas);

        //Gambi pra guardar o horimetro
        foreach ($respostas as $resposta) {
            $id_maquina = $resposta['id_maquina'];
            $unix_time = $resposta['unix_time'];
            if (!isset($maquinas_empresa_print[$id_maquina][$unix_time])) {
                if (end($maquinas_empresa_print[$id_maquina]) != $resposta['abastecimento_qtd']) {
                    $maquinas_empresa_print[$id_maquina][$unix_time] = $resposta['abastecimento_qtd'];
                    $maquina_horimetro[$id_maquina][$unix_time] = $resposta['horimetro'];
                    $maquina_timestamp[$id_maquina][$unix_time] = $resposta['abastecimento_timestamp'];
                }
            }
        }
        $json = array();
        echo "<pre>";
        foreach ($maquinas_empresa_print as $id_maquina => $maquina) {
            $dados_maquinas = $maquina['dados_maquina'];
            unset($maquina['dados_maquina']);
            foreach ($maquina as $timestamp => $qtd) {
                
                var_dump($id_maquina,$timestamp,$maquina_timestamp[$id_maquina][$timestamp]);
                $maquina_dados[$timestamp]['nome'] = $dados_maquinas['nome'];
                $maquina_dados[$timestamp]['data'] = date("d/m/Y H:i", $maquina_timestamp[$id_maquina][$timestamp]);
                $maquina_dados[$timestamp]['qtd'] = $qtd;
                $maquina_dados[$timestamp]['horimetro'] = $maquina_horimetro[$id_maquina][$timestamp];
            }
            if (isset($maquina_dados)) {
                $json = $json + $maquina_dados;
            }
        }
        
        ksort($maquina_dados, SORT_NUMERIC);
        $maquina_dados = array_values($maquina_dados);
        $data['resposta'] = json_encode($maquina_dados);
        $this->load->view("ajax/ajax_resposta", $data);
    }

}
