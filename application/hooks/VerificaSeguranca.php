<?php

class LoginSecure {

    function Verifica() {

        @session_start();

        if (!isset($_SESSION['id_usuario']) 
                && $_SERVER['REQUEST_URI'] != "/seguranca/login" 
                && !strpos($_SERVER['REQUEST_URI'],"servico")
                && $_SERVER['REQUEST_URI'] != "/ajax/ajax_seguranca/efetuar_login") {
            header("Location: /seguranca/login");
            die();
        }
    }

}
