<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Calculo_cerca
 *
 * @author Angelo
 */
class Calculos_maquinas_sis_antigo {

    protected $CI;

    function __construct() {
        $this->CI = & get_instance();
    }

    //put your code here
    function horas_trabalhadas($idRede) {

        $sistema_antigo = $this->CI->load->database("sistema_antigo", TRUE);

        $resposta = $sistema_antigo->query("SELECT R.relHora,R.relAZ,M.maqTU,maqAZ FROM relogio R,maquina M WHERE R.relIdRede = '$idRede' AND M.maqIdRede = '$idRede' ");

        $resultado = $resposta->result_array();
        $resultado = $resultado[0];

//        $Total_HoraTra = ($relHora + $maqTU) - ($relAZ + $maqAZ);
//        $Total_HoraTra = round($Total_HoraTra, 1);

        $horasTrabalhadas = ($resultado['relHora'] + $resultado['maqTU']) - ($resultado['relAZ'] + $resultado['maqAZ']);
        return round($horasTrabalhadas, 1);
    }

    function horas_trabalahdas_30dias($idRede) {

        $sistema_antigo = $this->CI->load->database("sistema_antigo", TRUE);
        $intervalo = time() - 2592000;

        $resposta = $sistema_antigo->query("SELECT MAX( graDifeUpdate ) as max_update , MIN( graDifeUpdate ) as min_update FROM grafico WHERE graDifeUpdate >$intervalo AND graIdRede = '$idRede'");
        $tempos = current($resposta->result_array());

        $resposta = $sistema_antigo->query("SELECT graHoraTra FROM grafico WHERE graIdRede = '$idRede' AND graDifeUpdate = '{$tempos['max_update']}' ");
        $resposta = $resposta->result_array();
        $max_tempo = $resposta[0]['graHoraTra'];

        $resposta = $sistema_antigo->query("SELECT graHoraTra FROM grafico WHERE graIdRede = '$idRede' AND graDifeUpdate = '{$tempos['min_update']}' ");
        $resposta = $resposta->result_array();
        $min_tempo = $resposta[0]['graHoraTra'];

        return $max_tempo - $min_tempo;
    }

    function horas_efetivas_30dias($idRede) {

        $sistema_antigo = $this->CI->load->database("sistema_antigo", TRUE);
        $intervalo = time() - 2592000;

        $resposta = $sistema_antigo->query("SELECT MAX( graDifeUpdate ) as max_update , MIN( graDifeUpdate ) as min_update FROM grafico WHERE graDifeUpdate >$intervalo AND graIdRede = '$idRede'");
        $tempos = current($resposta->result_array());

        $resposta = $sistema_antigo->query("SELECT graHoraEfetiva FROM grafico WHERE graIdRede = '$idRede' AND graDifeUpdate = '{$tempos['max_update']}' ");
        $resposta = $resposta->result_array();
        $max_tempo = $resposta[0]['graHoraEfetiva'];

        $resposta = $sistema_antigo->query("SELECT graHoraEfetiva FROM grafico WHERE graIdRede = '$idRede' AND graDifeUpdate = '{$tempos['min_update']}' ");
        $resposta = $resposta->result_array();
        $min_tempo = $resposta[0]['graHoraEfetiva'];

        return $max_tempo - $min_tempo;
    }

    function indice_de_utilizacao($idRede = 1047840160) {

        // Indice de Utilização Horas_trabalahdas / HH

        $sistema_antigo = $this->CI->load->database("sistema_antigo", TRUE);

        $resposta = $sistema_antigo->query("SELECT M.maqHHorizonte FROM maquina M WHERE M.maqIdRede = '$idRede' ");

        $resultado = $resposta->result_array();
        $hora_horizonte = $resultado[0]['maqHHorizonte'] == 0 ? 720 : $resultado[0]['maqHHorizonte'];

        $horas = $this->horas_trabalahdas_30dias($idRede);

        return round($horas / $hora_horizonte, 2);
    }

    function calculo_temperatura($idRede) {

        $sistema_antigo = $this->CI->load->database("sistema_antigo", TRUE);

        $resultado = $sistema_antigo->query("SELECT relA FROM relogio WHERE relIdRede = '$idRede' ");

        $var = $resultado->result_array();
        $relA = $var[0]['relA'];

        $temperatura = 17.4750 * exp($relA * 0.002);
        $temperatura += 2;
        $temperatura = round($temperatura, 0);
        if ($temperatura < 40) {
            $temperatura = 39;
        }

        return $temperatura;
    }

    function calc_hora_efetiva_30dias($idRede) {

        $horas_efetivas = $this->horas_efetivas_30dias($idRede);
        $horas_trabalhadas = $this->horas_trabalahdas_30dias($idRede);
        
       

        if ($horas_trabalhadas > 0 && $horas_efetivas > 0) {
            $porcetagem_hora_efetiva = ( $horas_efetivas / $horas_trabalhadas );
        } else {
            $porcetagem_hora_efetiva = 0;
        }
        
         

        return round($porcetagem_hora_efetiva * 100, 0);
    }

}
