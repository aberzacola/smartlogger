<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Calculo_cerca
 *
 * @author Angelo
 */
class Calculo_cerca {

    //put your code here
    function dentro_cerca($ponto, $poligono) {
        $vertices = count($poligono);
        $c = false;

        for ($i = 0, $j = $vertices - 1; $i < $vertices; $j = $i++) {
            if (( ($poligono[$i][1] >= $ponto[1] ) != ($poligono[$j][1] >= $ponto[1]) ) &&
                    ($ponto[0] <= ($poligono[$j][0] - $poligono[$i][0]) * ($ponto[1] - $poligono[$i][1]) / ($poligono[$j][1] - $poligono[$i][1]) + $poligono[$i][0])
            ) {
                $c = !$c;
            }
        }

        return $c;
    }

    function processa_poligono($poligono) {

        $poligono = str_ireplace("POLYGON((", "", $poligono);
        $poligono = str_ireplace("))", "", $poligono);
        $poligono = explode(",", $poligono);

        foreach ($poligono as $key => $ponto) {
            $poligono[$key] = explode(" ", $ponto);
        }

        return $poligono;
    }

}
