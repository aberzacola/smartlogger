<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Calculo_cerca
 *
 * @author Angelo
 */
class Calculos_maquinas {

    //Calcula o indice de utilizacao
    // Horimetro / HH
    function indicie_utilizacao($horimetro, $horas_horizonte) {
        return ( $horimetro / $horas_horizonte ) * 100;
    }

    //Função para calular e formatar o texto de quanto minutos que a máquina atualizou
    function formata_data_atualizacao($data_atualizacao) {
        $time = strtotime($data_atualizacao);
        return date("d/m/Y - G:i",$time);
    }
    
     //Função para calular e formatar o texto de quanto minutos que a máquina atualizou
    function formata_data_atualizacao_to_timestamp($data_atualizacao) {
        $time = strtotime($data_atualizacao);
        return $time;
    }
    
    //Função para formatar o texto no relé para NA/NF
    function formata_status_rele($staus_rele) {
        if($staus_rele == 1){
            return "NF";
        }else{
            return "NA";
        }
    }

}
