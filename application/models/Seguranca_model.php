<?php

class Seguranca_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // pega o nome da maquina registrada no sistema antigo
    function verifica_usuario($email, $senha) {

        $this->db->where('email', $email);
        $this->db->where('senha', $senha);
        $this->db->select("id");
        $this->db->select("nome");
        $this->db->select("id_empresa");
        $this->db->select("global");
        $query = $this->db->get("tbl_usuarios");

        if ($query->num_rows() > 0) {
            return current($query->result_array());
        } else {
            return false;
        }
    }

}
