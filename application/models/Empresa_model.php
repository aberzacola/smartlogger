<?php

class Empresa_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_empresa($id_empresa) {
        $this->db->where('id', $id_empresa);
        $query = $this->db->get('tbl_empresas');

        $resultado = $query->result_array();

        return $resultado[0];
    }
    
    //Verifica se o ID já está cadastrado em nome de outro condutor
    public function cadastro($nome) {

        $this->db->where("nome", $nome);

        $dados = array("nome" => $nome, "lat_inicial" => 0, "lng_inicial" => 0, "zoom_inicial" => 0);


        $this->db->insert("tbl_empresas", $dados);
    }
    
    public function get_all_empresas(){
        
        $query = $this->db->get('tbl_empresas');

        $resultado = $query->result_array();

        return $resultado;
        
    }

}
