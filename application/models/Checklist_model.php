<?php

class Checklist_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function insere_checklist($dados) {
        $this->db->insert('tbl_checklist', $dados);
    }

    function get_last_checklist($id_empresa) {

        $this->db->where("id_empresa", $id_empresa);
        $this->db->limit(1);
        $this->db->order_by('id', 'DESC');
        $resposta = $this->db->get("tbl_checklist");


        return current($resposta->result_array());
    }
    
    function get_all_checklist_historico_maq_basico($id_maquina){
        
        $this->db->where("id_maquina", $id_maquina);
        $this->db->select("id");
        $this->db->select("DATE_FORMAT(datetime,'%d/%m/%Y %H:%i') as datetime");
        $this->db->order_by('id', 'DESC');
        $resposta = $this->db->get("tbl_checklist_historico");       
        
        return $resposta->result_array();
    }
    
    function get_checklist_historico_maq($id_checklist){
        
        $this->db->where("id", $id_checklist);
        $this->db->select("DATE_FORMAT(datetime,'%d/%m/%Y %H:%i') as datetime");
        $this->db->select("itens_criticos");
        $this->db->select("itens_gerais");
        $resposta = $this->db->get("tbl_checklist_historico");       
        
        return $resposta->result_array();
    }

}
