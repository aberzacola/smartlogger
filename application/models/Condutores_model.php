<?php

class Condutores_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //Verifica se o ID já está cadastrado em nome de outro condutor
    public function checa_id($id_identificador) {

        $this->db->where("id_identificador", $id_identificador);
        $query = $this->db->get("tbl_condutores");

        return $query->num_rows() == 0 ? FALSE : TRUE;
    }

    //Pega todos os condutores cadastrados para a empresa
    public function get_condutores_empresa($id_empresa) {

        $this->db->where("id_empresa", $id_empresa);

        $query = $this->db->get("tbl_condutores");

        return $query->result_array();
    }

    // Cadastra novo operador
    public function cadastra_operador($nome, $id_empresa, $tipoCondutor, $dataTreinamento, $dataExameMedico) {

        switch ($tipoCondutor) {
            case "condutor":
                $dados = array("nome" => $nome, "id_empresa" => $id_empresa, "condutor" => 1, "data_exame_treinamento" => $dataTreinamento, "data_exame_medico" => $dataExameMedico);
                break;
            case "abastecedor":
                $dados = array("nome" => $nome, "id_empresa" => $id_empresa, "abastecedor" => 1, "data_exame_treinamento" => $dataTreinamento, "data_exame_medico" => $dataExameMedico);
                break;
            case "mecanico":
                $dados = array("nome" => $nome, "id_empresa" => $id_empresa, "mecanico" => 1, "data_exame_treinamento" => $dataTreinamento, "data_exame_medico" => $dataExameMedico);
                break;
        }


        $this->db->insert("tbl_condutores", $dados);
    }

    public function get_condutor_nome($id_identificador) {

        $this->db->where("id", $id_identificador);
        $this->db->select("nome");
        $query = $this->db->get("tbl_condutores");

        return current($query->result_array());
    }

    public function get_grupo_condutor_nome($ids_array) {

        $this->db->where_in("id", $ids_array);
        $this->db->select("id,nome");
        $query = $this->db->get("tbl_condutores");

        return $query->result_array();
    }

    //Deleta máquina
    public function deleta_condutor($id_condutor) {

        $this->db->where("id", $id_condutor);
        return $this->db->delete("tbl_condutores");
    }
    
    //Pega as transmissões a partir da data dada_inicio como parâmtro até a data final
    public function get_all_trans_between($id_condutor, $data_inicio, $data_final) {

        //Essa query pega todos as trans
        $resultado = $this->db->query("SELECT * FROM tbl_transmissao WHERE id_condutor = $id_condutor  AND timestamp_transmissao > '$data_inicio' AND timestamp_transmissao < '$data_final'");
        return $resultado->result_array();
    }

}
